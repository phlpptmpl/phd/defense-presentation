%!TEX root=../../source.tex
%!TEX file=content/appendix/tools.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Tools}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Gauss-Legendre Quadrature: \citep{Gilliam.1992,Abramowitz.1972}}
  
  \begin{minipage}[c]{0.49\linewidth}
    Approximation of a function $f \of{x}$ by polynomials of degree $\polydeg = 2 \, \nquadraturepoints - 1$
    
    Associated orthogonal polynomials are Legendre polynomials
    \begin{align*}
      \int\limits_{-1}^{1}{
        f \of{x} \,
        \td{x}
      }
        & \approxeq
          \sum\limits_{\loopindex = 1}^{\nquadraturepoints}{
            w_{\loopindex} \,
            f \of*{ x_{\loopindex} }
          }
    \end{align*}
    
    Or rewritten over interval $\interval{a}{b}$
    \begin{align*}
      \int\limits_{a}^{b}{
        f \of{x} \,
        \td{x}
      }
        & \approxeq
          \frac{
            b
            -
            a
          }{
            2
          } \,
          \sum\limits_{\loopindex = 1}^{\nquadraturepoints}{
            w_{\loopindex} \,
            f \of*{
              \frac{
                b
                -
                a
              }{
                2
              } \,
              x_{\loopindex}
              +
              \frac{
                a
                +
                b
              }{
                2
              }
            } \,
            \td{x}
          }
    \end{align*}
    
    Weight $w_{\loopindex}$ is calculated from roots of the $\nquadraturepoints$-th polynomial $P_{\nquadraturepoints}$
    %
    \begin{align*}
      w_{\loopindex}
        & =
          \frac{
            2
          }{
            \parentheses*{
              1
              -
              x_{\loopindex}^{2}
            } \,
            \pow[2]{
              \left[
                \p{P}_{\nquadraturepoints} \of{ x_{\loopindex} }
              \right]
            }
          }
    \end{align*}
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      \includegraphics[%
          width=1.00\linewidth,%
          axisratio=1.00,%
        ]{quadrature-sketch}
    \end{figure}
  \end{minipage}%
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Newton-Raphson Method}
  
  \begin{minipage}[c]{0.49\linewidth}
    Assuming function $f \colon \parentheses*{a, b} \to \mathds{R}$ differentiable on interval $\interval{a}{b}$
    
    Given current approximate of root $\itervar*_{\iterindex}$, its Taylor approximation reads
    %
    \begin{align*}
      y
        & = f \of{ \itervar*_{\iterindex}}
            +
            \p{f} \of{ \itervar*_{\iterindex} } \,
            \parentheses*{
              \itervar*
              -
              \itervar*_{\iterindex}
            }
    \end{align*}
    %
    yielding next estimate $\itervar*_{\iterindex + 1}$ (at root i.e., $y = 0$)
    %
    \begin{align*}
      \itervar*_{\iterindex + 1}
        & = \itervar*_{\iterindex}
            -
            \frac{
              1
            }{
              \p{f} \of{\itervar*_{\iterindex}}
            }
            \,
            f \of{\itervar*_{\iterindex}}
    \end{align*}
    
    Termination criterion are
    %
    \begin{itemize}
      \item Small step size $\abs{ \itervar*_{\iterindex + 1} - \itervar*_{\iterindex} } \leq \threshold_{\iterstep}$
      \item Small residual $\abs{ f \of{\itervar*_{\iterindex}} } \leq \threshold_{f}$
      \item Excessive iteration count $\iterindex > \iterindex_{\ms{max}}$
    \end{itemize}
  \end{minipage}%
  \hfill
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      \includegraphics[%
          width=1.00\linewidth,%
          axisratio=1.50,%
        ]{newton-raphson}
    \end{figure}
    
    \begin{table}
      \centering
      \smaller[2]
      \pgfplotstabletypeset[%
          %
          col sep=comma,%
          columns={k,xk,fxk},%
          columns/k/.style={%
            column name={$\iterindex$},%
          },%
          columns/xk/.style={%
            column name={$\itervar*_{\iterindex}$},%
          },%
          columns/fxk/.style={%
            column name={$f \of{ \itervar*_{\iterindex} }$},%
          },%
          % Style table
          every head row/.style={%
            before row=\toprule,%
            after row=\midrule,%
          },%
          precision=6,%
          %
          % Style columns
        ]{data/newton-raphson/iterations.dat}
    \end{table}
  \end{minipage}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Branches of Mechanics}
  
  \begin{minipage}[t]{0.49\linewidth}
    \begin{block}{Kinematics}
      \vspace*{0.50\baselineskip}
      \begin{itemize}
        \item describes motion of points (bodies or systems of bodies)
        \item often referred to as "geometry of motion"
        \item rigid transformations describe movement
        \item disregards how forces act on bodies
      \end{itemize}
    \end{block}
  \end{minipage}%
  \hfill%
  \begin{minipage}[t]{0.49\linewidth}
    \begin{block}{Kinetics}
      \vspace*{0.50\baselineskip}
      \begin{itemize}
        \item relates motion and its causes, forces and torques
        \item superseded by term ``dynamics''
      \end{itemize}
    \end{block}
  \end{minipage}
  
  \begin{minipage}[t]{0.49\linewidth}
    \begin{block}{Statics}
      \vspace*{0.50\baselineskip}
      \begin{itemize}
        \item kinetics under the assumption of zero acceleration
        \item usually system is assumed at rest
        \item may also be moving at constant velocity
      \end{itemize}
    \end{block}
  \end{minipage}%
  \hfill%
  \begin{minipage}[t]{0.49\linewidth}
    \begin{block}{Dynamics}
      \vspace*{0.50\baselineskip}
      \begin{itemize}
        \item second pillar to classical mechanics besides \emph{kinematics}
        \item relates motion of bodies and its causes
        \item particularly mass and moment of inertia
      \end{itemize}
    \end{block}
  \end{minipage}%
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Formulation of Classical Mechanics}
  
  \begin{minipage}[t]{0.325\linewidth}
    \begin{block}{Newtonian Mechanics}
      \vspace*{0.50\baselineskip}
      \begin{enumerate}
        \item An object remains at rest or continues moving at constant velocity, unless acted upon by a force
        \item The vector sum of forces $\vect{f}$ equals the bodies mass $\mass$ times its acceleration $\linacc$ i.e., $\vect{f} = \mass \, \linacc$
        \item Body A exerting force $\vect{f}_{\ms{A}}$ on body B experiences the same force in opposite direction $\vect{f}_{\ms{A}} = \vect{f}_{\ms{B}}$
      \end{enumerate}%
      \vspace*{-1.00\baselineskip}%
      \begin{align*}
        \vect{f}
          & = \mass \,
              \linacc \,
              ,
            \\
        \vect{\tau}
          & = \inertiatensor \,
              \angacc
              +
              \crossp{
                \angvel
              }{
                \inertiatensor \,
                \angvel
              } \,
              .
      \end{align*}
    \end{block}
  \end{minipage}%
  \hfill%
  \begin{minipage}[t]{0.325\linewidth}
    \begin{block}{Lagrangian Mechanics}
      \vspace*{0.50\baselineskip}
      Trajectory of a system of bodies derived by
      \begin{enumerate}
        \item treating constraints explicitly through Lagrange multipliers
        \item incorporating constraints by choice of generalized coordinates
      \end{enumerate}%
      \vspace*{-1.00\baselineskip}%
      \begin{align*}
        \pd{
          \lagrangian
        }{
          \linpos_{\iterindex}
        }
        -
        \od{
        }{
          t
        } \,
        \pd{
          \lagrangian
        }{
          \dot{\linpos}_{\iterindex}
        }
        +
        \sum\limits_{\loopindex}^{C}{
          \lagrangemultip_{\loopindex} \,
          \pd{
            f_{\loopindex}
          }{
            \linpos_{\iterindex}
          }
        }
          & = 0 \,
              ,
          \\
        \od{
        }{
          t
        } \,
        \pd{
          \lagrangian
        }{
          \dotgenq*_{\iterindex}
        }
        -
        \pd{
          \lagrangian
        }{
          \genq*_{\iterindex}
        }
        +
        \pd{
          \energydissipative
        }{
          \dotgenq*_{\iterindex}
        }
          & = 0 \,
              .
      \end{align*}
    \end{block}
  \end{minipage}
  \hfill%
  \begin{minipage}[t]{0.325\linewidth}
    \begin{block}{Hamiltonian Mechanics}
      \vspace*{0.50\baselineskip}
      \begin{itemize}
        \item Describes system in canonical coordinates $\vect{r} = \parentheses*{ \vect{q}, \vect{p} }$
        \item Spaces of $\vect{q}$ and $\vect{p}$ are cotangent 
        \item generalized momentum $\vect{p}$ and generalized positions $\vect{q}$
      \end{itemize}%
      \vspace*{-1.00\baselineskip}%
      \begin{align*}
        \od{
          \vect{p}
        }{
          t
        }
          & = -
              \pd{
                \hamiltonian
              }{
                \vect{q}
              } \,
          & \od{
              \vect{q}
            }{
              t
            }
          & = +
              \pd{
                \hamiltonian
              }{
                \vect{p}
              } \,
              .
      \end{align*}
    \end{block}
  \end{minipage}%
\end{frame}
