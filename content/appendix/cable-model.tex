%!TEX root=../../source.tex
%!TEX file=content/appendix/cable-model.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Cable Model}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Different Beam Theories}
  
  \begin{minipage}[t]{0.32\linewidth}
    \alert{Euler-Bernoulli}
    
    \begin{itemize}
      \item Small deflection of beam
      \item Subject to lateral load only
      \item Cross section remains normal to neutral axis during deformation
      \item Special case of Timoshenko beam
    \end{itemize}
    
    Static equilibrium
    \begin{align*}
      \elasticity \,
      \secondmomentofarea \,
      \od[4]{
        \wavepertubation
      }{
        \pathcoord
      }
        & = q \of{ \pathcoord } \,
            .
    \end{align*}
  \end{minipage}%
  \hfill%
  \begin{minipage}[t]{0.32\linewidth}
    \alert{Timoshenko}
    
    \begin{itemize}
      \item Accounts for shear deformation and rotational bending
      \item Cross section of beam can rotate during deformation
      \item Mathematically reduces stiffness of beam yielding larger deflection
    \end{itemize}
    
    Static equilibrium
    \begin{align*}
      \elasticity \,
      \secondmomentofarea \,
      \od[4]{
        \wavepertubation
      }{
        \pathcoord
      }
        & = q \of{ \pathcoord }
            -
            \frac{
              \elasticity \,
              \secondmomentofarea
            }{
              k \,
              \crosssection \,
              G
            } \,
            \od[2]{
              q
            }{
              \pathcoord
            } \,
            .
    \end{align*}
  \end{minipage}%
  \hfill%
  \begin{minipage}[t]{0.32\linewidth}
    \alert{Cosserat Rod - Elastica Theory}
    
    \begin{itemize}
      \item Large deformations allowed
      \item No constraints on normals of cross section
      \item Accounts for strain and shear forces and bending moments
      \item Defined through the neutral axis and three directors
    \end{itemize}
  \end{minipage}%
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Mechanical Model for Evaluation}
  
  \begin{minipage}[c]{0.49\linewidth}
    Lagrangian of Euler-Bernoulli beam is
    %
    \begin{align*}
      \lagrangian
        & \coloneqq
            \frac{
              1
            }{
              2
            } \,
            \mu \,
            \pow*[2]{
              \pd{
                \wavepertubation
              }{
                t
              }
            }
            -
            \frac{
              1
            }{
              2
            } \,
            \elasticity* \,
            \secondmomentofarea \,
            \pow*[2]{
              \pd[2]{
                \wavepertubation
              }{
                \pathcoord
              }
            }
            +
            q \of{ \pathcoord } \,
            \wavepertubation \of{\pathcoord, t}
    \end{align*}
    %
    from which follow the Euler-Lagrange equations
    %
    \begin{align*}
      \pd[2]{
      }{
        \pathcoord
      } \of*{
        \elasticity \,
        \secondmomentofarea \,
        \pd[2]{
          \wavepertubation
        }{
          \pathcoord
        }
      }
        & = - \mu \,
            \pd[2]{
              \wavepertubation
            }{
              t
            }
            +
            q \of{ \pathcoord }
    \end{align*}
    
    Fourier decomposition into harmonic vibrations%
    %
    \begin{align*}
      \wavepertubation \of{\pathcoord, t}
        & = \Re \cof*{
              \hat{\wavepertubation} \,
              \exp \of{
                -\imagu \,
                \angularfrequency \,
                t
              }
            }
    \end{align*}
    %
    yields solving ordinary differential equation%
    %
    \begin{align*}
      \elasticity \,
      \secondmomentofarea \,
      \od[4]{
        \hat{\wavepertubation}
      }{
        \pathcoord
      }
      +
      \mu \,
      \pow[2]{
        \angularfrequency
      } \,
      \hat{\wavepertubation}
        & = 0
    \end{align*}
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      \includegraphics[%
          width=\linewidth,%
        ]{simply-supported-beam}
    \end{figure}
    
    General solution is
    %
    \begin{align*}
      \hat{\wavepertubation}
        & = A_{1} \,
            \cosh \of{ \beta \, \pathcoord }
            +
            A_{2} \,
            \sinh \of{ \beta \, \pathcoord }
            +
            \dotsb
        \\
        & \phantom{=}
            +
            A_{3}
            \cos \of{ \beta \, \pathcoord }
            +
            A_{4}
            +
            \sin \of{ \beta \, \pathcoord } \,
            ,
          \\
      \beta
        & \coloneqq
          \pow*[\nicefrac{1}{4}]{
            \frac{
              \mu \,
              \pow[2]{
                \angularfrequency
              }
            }{
              \elasticity \,
              \secondmomentofarea
            }
          }
    \end{align*}
  \end{minipage}%
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Mechanical Measures}
  
  \begin{minipage}[t]{0.49\linewidth}
    Physical measures for
    \begin{itemize}
      \item strain
        \begin{align*}
          \strainmeasure
            & = \pd{
                  \curve
                }{
                  \lpathcoord
                }
                -
                \crossp{
                  \rotvector
                }{
                  \tangent
                }
              = \begin{bmatrix}
                  \strainmeasure*_{\subtangent}
                    \\
                  \strainmeasure*_{\subnormal}
                    \\
                  \strainmeasure*_{\subbinormal}
                \end{bmatrix} \,
        \end{align*}
      \item bending
        \begin{align*}
          \bendingmeasure
            & = \pd{
                  \rotvector
                }{
                  \lpathcoord
                }
              % = \p{\rotvector}
              = \begin{bmatrix}
                  \bendingmeasure*_{\subtangent}
                    \\
                  \bendingmeasure*_{\subnormal}
                    \\
                  \bendingmeasure*_{\subbinormal}
                \end{bmatrix} \,
                .
        \end{align*}
    \end{itemize}
  \end{minipage}%
  \hfill%
  \begin{minipage}[t]{0.49\linewidth}
    Constitutive material laws
      \begin{align*}
        \vect{N}%\stress
          & = \begin{bmatrix}
                \youngsmodulus \,
                  \crosssection_{\subtangent}
                  & 0
                  & 0
                  \\
                0
                  & \shearmodulus \,
                    \crosssection_{\subnormal}
                  & 0
                  \\
                0
                  & 0
                  & \shearmodulus \,
                    \crosssection_{\subbinormal}
              \end{bmatrix}
              \,
              \begin{bmatrix}
                \strainmeasure*_{\subtangent}
                  \\
                \strainmeasure*_{\subnormal}
                  \\
                \strainmeasure*_{\subbinormal}
              \end{bmatrix}
            = \matr{C}^{\strainmeasure*} \,
              \strainmeasure \,
              ,
          \\
        \vect{M}%\torque
          & = \begin{bmatrix}
                \shearmodulus \,
                  \secondmomentofarea_{\subtangent}
                  & 0
                  & 0
                  \\
                0
                  & \youngsmodulus \,
                    \secondmomentofarea_{\subnormal}
                  & 0
                  \\
                0
                  & 0
                  & \youngsmodulus \,
                    \secondmomentofarea_{\subbinormal}
              \end{bmatrix}
              \,
              \begin{bmatrix}
                \bendingmeasure*_{\subtangent}
                  \\
                \bendingmeasure*_{\subnormal}
                  \\
                \bendingmeasure*_{\subbinormal}
              \end{bmatrix}
            = \matr{C}^{\bendingmeasure*} \,
              \bendingmeasure \,
              ,
      \end{align*}
  \end{minipage}%
\end{frame}
