%!TEX root=../../source.tex
%!TEX file=content/appendix/stress-strain-dynamics.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Stress-Strain Dynamics}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{A Real Problem}

  \begin{minipage}[t]{0.49\linewidth}%
    \vspace*{-0.25\baselineskip}%
    \begin{figure}
      \centering
      \smaller[2]
      % \tikzexternalenable%
      % \tikzsetnextfilename{stress-strain--stress-strain-real}
      \includegraphics[%
          width=\linewidth,%
          axisratio=1.50,%
        ]{stress-strain/stress-strain-real}
      % \tikzexternaldisable%
    \end{figure}
  \end{minipage}%
  \hfill%
  \begin{minipage}[t]{0.49\linewidth}
    Observations
    \begin{itemize}
      \item \alert{ideal cable}: Hookean solid + Newtonian fluid
        \begin{itemize}
          \item instantaneous response
          \item stateless behavior
        \end{itemize}
      \item \alert{physical cable}: viscoelastic behavior
        \begin{itemize}
          \item retarded response: relaxation/creep
          \item short or long-term behavior
        \end{itemize}
    \end{itemize}
    
    Conventional approaches to $\stress*$-$\strain*$ dynamics
    %
    \vspace*{-0.25\baselineskip}%
    \begin{itemize}
      \item Hookean solid + Newtonian fluid
      \item Superimposed compensation function on ideal cable \citep{Miermeister.2015}
      \item Approximated by identified strain/stress-step response \citep{Piao.2018}
    \end{itemize}
  \end{minipage}%
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Rheological Considerations}

  \begin{minipage}[t]{0.49\linewidth}
    Rheology-based model composed of
    \begin{description}
      \item[spring] instantaneous elasticity
      \item[damper] retarded elasticity
    \end{description}
    
    Combine basic components into
    %
    \begin{itemize}
      \item \alert{serial} chain of \alert{parallel} spring-damper%
        %
        \vspace*{-0.25\baselineskip}%
        \begin{align*}
           \strain*
            & = \strain* \of{\stress*, \dotstress*, \ddotstress*, \dotso}
        \end{align*}
      \item \alert{parallel} chain of \alert{serial} spring-damper%
        %
        \vspace*{-0.25\baselineskip}%
        \begin{align*}
           \stress*
            & = \stress* \of{\strain*, \dotstrain*, \ddotstrain*, \dotso}
        \end{align*}
    \end{itemize}
    
    \vfill
    
    \begin{takeawayblock}{}
      Base on parallel chain of serial spring-damper; identify how many elements are needed
    \end{takeawayblock}
    
    % % \begin{minipage}[t]{0.49\linewidth}
    %   \begin{figure}
    %     \centering
    %     \includegraphics[
    %         width=\linewidth,%
    %       ]{material/generalized-kelvin}
    %   \end{figure}
    % % \end{minipage}%
    % % \hfill%
    % % \begin{minipage}[t]{0.49\linewidth}
    %   \begin{figure}
    %     \centering
    %     \includegraphics[
    %         width=\linewidth,%
    %       ]{material/generalized-maxwell}
    %   \end{figure}
    % % \end{minipage}
  \end{minipage}%
  \hfill%
  \begin{minipage}[t]{0.49\linewidth}%
    \vspace*{-0.50\baselineskip}%
    \alert{Generalized Kelvin Chain}%
    %
    \vspace*{-0.25\baselineskip}%
    \begin{figure}
      \centering
      \smaller[2]
      \includegraphics[
          width=\linewidth,%
        ]{material/generalized-kelvin}
    \end{figure}
    
    \alert{Generalized Maxwell Chain}%
    %
    \vspace*{-0.25\baselineskip}%
    \begin{figure}
      \centering
      \smaller[2]
      \includegraphics[
          % width=\linewidth,%
          height=0.30\textheight,%
        ]{material/generalized-maxwell}
    \end{figure}
  \end{minipage}%
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Parameter Identification}

  \begin{minipage}[c]{0.49\linewidth}
    \alert{Dynamics} formulation of strain-stress%
    %
    \vspace*{-0.25\baselineskip}%
    \begin{align*}
      \stress*_{\msmaxwell}
        & = \parentheses*{
              \youngsmodulus_{1}
              +
              \sum\limits_{\loopindex}^{n_{\msmaterial}}{
                \frac{
                  \dif
                }{
                  \frac{
                    \dif
                  }{
                    \youngsmodulus_{\loopindex}
                  }
                  +
                  \frac{
                    1
                  }{
                    \viscousmodulus_{\loopindex}
                  }
                }
              } \,
            } \,
            \strain*_{\msmaxwell}
    \end{align*}
    
    Perform parameter estimation using \alert{transfer function} of dynamics ODE
    %
    \vspace*{-0.25\baselineskip}%
    \begin{align*}
      \tf \of{\s}
        & =
            \frac{
              \Sigma \of{\s}
            }{
              E \of{\s}
            }
          =
            \frac{
              b_{ n_{b} } \,
              \pow[n_{b}]{\s}
              +
              \dotsb
              +
              b_{1} \,
              \s
              +
              b_{0}
            }{
              a_{ n_{a} } \,
              \pow[n_{a}]{\s}
              +
              \dotsb
              +
              a_{1} \,
              \s
              +
              a_{0}
            } \,
            ,
    \end{align*}
    
    Apply stress-step input, record strain output
    
    Parameter identification formulated as \alert{least-squares problem}, solved using S-K iteration
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      % \tikzexternalenable%
      % \tikzsetnextfilename{stress-strain--stress-relaxation}
      \includegraphics[%
          % width=1.00\linewidth,%
          % axisratio=1.00,%
        ]{stress-strain/estimation/varying-nelements}
      % \tikzexternaldisable%
    \end{figure}
  \end{minipage}%

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Estimated Parameters}

  \begin{figure}
    \centering
    \smaller[2]
    % \tikzexternalenable%
    % \tikzsetnextfilename{stress-strain--estimation--mechanical-parameters}
    \includegraphics[%
      ]{stress-strain/estimation/mechanical-parameters}
    % \tikzexternaldisable%
  \end{figure}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Prediction of Stress-Strain Dynamics}

  \begin{figure}
    \centering
    \smaller[2]
    \includegraphics[
      ]{stress-strain/estimation/step-responses}
  \end{figure}
  
  % \vfill
  
  % \begin{takeawayblock}{}
  %   Maxwell-chain-based stress-strain model captures relaxation peak well with a minimum of only $n_{\msmaxwell} = 3$ elements
  % \end{takeawayblock}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Generalized Viscoelastic Material Models}
  
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      \includegraphics[
          width=\linewidth,%
          % height=0.50\textheight,%
          axisratio=1.00,%
        ]{material/generalized-maxwell}
    \end{figure}
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      \includegraphics[
          width=\linewidth,%
          % height=0.50\textheight,%
          axisratio=1.00,%
        ]{material/generalized-kelvin}
    \end{figure}
  \end{minipage}%
  
  \begin{minipage}[c]{0.49\linewidth}
    Dynamics equation: strain $\strain*$ to stress $\stress*$
    \begin{align*}
      \stress*_{\msmaxwell}
        & = \parentheses*{
              \youngsmodulus_{1}
              +
              \sum\limits_{\loopindex}^{n_{\msmaterial}}{
                \frac{
                  \dif
                }{
                  \frac{
                    \dif
                  }{
                    \youngsmodulus_{\loopindex}
                  }
                  +
                  \frac{
                    1
                  }{
                    \viscousmodulus_{\loopindex}
                  }
                }
              } \,
            } \,
            \strain*_{\msmaxwell}
    \end{align*}
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    Dynamics equation: stress $\stress*$ to strain $\strain*$
    \begin{align*}
      \strain*_{\mskelvinvoigt}
        & = \parentheses*{
              \frac{
                1
              }{
                \youngsmodulus_{1}
              }
              +
              \sum\limits_{\loopindex}^{n_{\msmaterial}}{
                \frac{
                  1
                }{
                  \youngsmodulus_{\loopindex}
                  +
                  \viscousmodulus_{\loopindex} \,
                  \dif \,
                }
              }
            } \,
            \stress*_{\mskelvinvoigt} \,
    \end{align*}
  \end{minipage}%
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Step Functions of Most Common Viscoelastic Material Models}
  
  \begin{minipage}[b]{0.32\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      \includegraphics[%
          width=1.00\linewidth,%
          % height=0.20\textheight,%
          % axisratio=1.00,%
        ]{material/maxwell/schematic_eps}
    \end{figure}
  \end{minipage}%
  \hfill%
  \begin{minipage}[b]{0.32\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      \includegraphics[%
          width=1.00\linewidth,%
          % height=0.20\textheight,%
          % axisratio=1.00,%
        ]{material/kelvinvoigt/schematic_eps}
    \end{figure}
  \end{minipage}%
  \hfill%
  \begin{minipage}[b]{0.32\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      \includegraphics[%
          width=1.00\linewidth,%
          % height=0.20\textheight,%
          % axisratio=1.00,%
        ]{material/zener/schematic_eps}
    \end{figure}
  \end{minipage}
  
  \begin{minipage}[t]{0.32\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      \includegraphics[%
          width=1.00\linewidth,%
          % height=0.20\textheight,%
          axisratio=1.00,%
        ]{material/maxwell/step-responses}
    \end{figure}
  \end{minipage}%
  \hfill%
  \begin{minipage}[t]{0.32\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      \includegraphics[%
          width=1.00\linewidth,%
          % height=0.20\textheight,%
          axisratio=1.00,%
        ]{material/kelvinvoigt/step-responses}
    \end{figure}
  \end{minipage}%
  \hfill%
  \begin{minipage}[t]{0.32\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      \includegraphics[%
          width=1.00\linewidth,%
          % height=0.20\textheight,%
          axisratio=1.00,%
        ]{material/zener/step-responses}
    \end{figure}
  \end{minipage}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Prediction per Model Complexity}
\end{frame}
