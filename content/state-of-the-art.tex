%!TEX root=../source.tex
%!TEX file=content/spatial-dynamics.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Literature Review}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Superimposed Linear Wave Equation -- FAST cable robot \citep{Liu.2013}}

  \begin{minipage}[c]{0.49\linewidth}
    \begin{itemize}
      \item Assume cable in static equilibrium (\citeauthor{Irvine.1981}'s catenary model)
      \item Superimpose linear wave equation
        \begin{itemize}
          \item \alert{small} deflections from neutral axis
          \item tension \alert{constant} along cable
          \item governing equations of \alert{longitudinal} cable motion
            \begin{align*}
              \pd[2]{
                \wavepertubation \of{\pathcoord, t}
              }{
                t
              }
                & = \pow[2]{ \wavespeed } \,
                    \pd[2]{
                      \wavepertubation \of{\pathcoord, t}
                    }{
                      \pathcoord
                    }
            \end{align*}
        \end{itemize}
      \item Suitable for static or quasi-static simulation
    \end{itemize}
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \includegraphics[%
          % width=\linewidth,%
          height=0.60\textheight,%
          axisratio=1.00,%
        ]{references/liu2013}
    \end{figure}
  \end{minipage}%

  \vfill

  \begin{takeawayblock}{}
    Purely kinetostatics model with limitations due to assumptions of linear wave equation
  \end{takeawayblock}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{\secname}
  \framesubtitle{Rigid Finite Element Method \citep{Kamman.1999,Tempel.2018c}}

  \begin{minipage}[c]{0.49\linewidth}
    \begin{itemize}
      \item Split cable into \alert{$\rfemnumsegments$ segments} with
        \begin{itemize}
          \item \alert{elasticity}: linear spring-damper
          \item \alert{flexibility}: rotary spring-damper
        \end{itemize}
      \item Largely \alert{coupled} dynamics system
        \begin{align*}
          \rfemofsegment{\linpos}{\rfemsegmentindex}
            & = \rfemproximal
                +
                \sum\limits_{l = 0}^{\rfemsegmentindex - 1}{
                  \parentheses*{
                    \rfemofsegment{\rfembodylength}{l}
                    +
                    \rfemofsegment{\rfemgenqelong*}{l}
                  } \,
                  % \rfemofsegment{\rfemgenqstrain*}{j} \,
                  % \rfemofsegment{\rfembodylength}{j} \,
                  \begin{bmatrix}
                    \cos \rfemofsegment{\rfemgenqrotation*}{l}
                      \\
                    \sin \rfemofsegment{\rfemgenqrotation*}{l}
                  \end{bmatrix}
                }
        \end{align*}
        % \begin{itemize}
        %   \item Dense mass matrix
        %   \item \alert{expensive} evaluation
        % \end{itemize}
      \item \alert{Small} segment length needed for good approximation
      \item Incorporation of \alert{change of length} requires dynamic re-segmentation
      % \item Guiding introduces \alert{artifical vibration}
    \end{itemize}
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      % \tikzexternalenable%
      % \tikzsetnextfilename{kinematics--segmentized--segment}
      \includegraphics[%
          width=\linewidth,%
          % height=\linewidth,%
        ]{kinematics/segmentized/segment}
      % \tikzexternaldisable
    \end{figure}%
    
    \begingroup
    \centering
    \begin{itemize}
      \item[{\tikz{\sde[type=linear,radius=0.50em,transform shape,fill=White,]{(0.00,0.00)};}}] linear spring-damper
      \item[{\tikz{\sde[type=rotational,radius=0.50em,transform shape,fill=White,]{(0.00,0.00)};}}] rotary spring-damper
    \end{itemize}
    \endgroup
  \end{minipage}%

  \vfill

  \begin{takeawayblock}{}
    Formulation unsuitable for dynamics simulation of cable robots due to spatial discretization
  \end{takeawayblock}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Continuum Approximation -- Rayleigh-Ritz \citep{AyalaCuevas.2018,Tempel.2019b}}

  \begin{minipage}[c]{0.59\linewidth}
    \begin{itemize}
      \item Cable split into two parts
        \begin{enumerate}
          \item Coiled cable (ideal coiling)
          \item Free cable (in workspace)
        \end{enumerate}
      \item Describe cable deflection through \alert{shape functions}%
        \begin{align*}
          \virtual{z} \of{x, t}
            & = \sum\limits_{\loopindex = 1}^{N}{
                  x^{\loopindex + 1} \,
                  % \shapefuns_{\loopindex} \of{x} \,
                  \genq_{\loopindex} \of{t}
                } \,
                  .
                % \\
          % \shapefuns_{\loopindex}
          %   & = x^{\loopindex + 1}
        \end{align*}
      \item Geometric deflection implies \alert{no elasticity}
        \begin{align*}
          \pow*[2]{
            \pd{
              \virtual{x}
            }{
              x
            }
            +
            1
          }
          +
          \pow*[2]{
            \pd{
              \virtual{z}
            }{
              x
            }
          }
            & = 1
        \end{align*}
      \item Improved version including elasticity and flexibility provided by \citet{Tempel.2019b}
    \end{itemize}
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.39\linewidth}
    \begin{figure}
      \centering
      \includegraphics[%
          % width=\linewidth,%
          height=0.45\textheight,%
          axisratio=1.00,%
       ]{references/ayalacuevas2018}
    \end{figure}
  \end{minipage}%

  \vfill

  \begin{takeawayblock}{}
    Dereliction of cable elasticity and flexibility through imposing geometric constraint
  \end{takeawayblock}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[standout]
  
  Classical Mechanics comes to the rescue
  
  (and Euler, Bernoulli, Timoshenko, and Cosserat)
\end{frame}
