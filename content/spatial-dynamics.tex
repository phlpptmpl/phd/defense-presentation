%!TEX root=../source.tex
%!TEX file=content/spatial-dynamics.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Modeling of Spatial Cable Dynamics}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Assumptions \& Questions to Answer}
  
  \begin{minipage}[t]{0.49\linewidth}
    \alert{Assumptions:} Cable \ldots
    
    % Cable \dotso%
    \begin{itemize}
      \item can undergo large deformation
      \item deforms like Hookean solid: $\stress* = \elasticity* \, \strain*$
      \item of solid cirular cross-section
      \item interacts only with platform, no additional interaction
    \end{itemize}
  \end{minipage}%
  \hfill%
  \begin{minipage}[t]{0.49\linewidth}
    \alert{Questions to answer:} how to \ldots
    
    % \vspace*{1.00\baselineskip}
    
    % \tableofcontents[%
    %     sectionstyle=hide/hide,%
    %     subsectionstyle=show/show/hide,%
    %   ]
    \begin{enumerate}
      \item describe curve geometrically
      \item measure curve deformation
      \item discretize curve
      \item formulate constrained dynamics
      \item incorporate time-varying length
    \end{enumerate}
  \end{minipage}%
  \hfill%
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Geometric Description of Curve}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname: Cosserat Rod~\citep{Soler.2018,Zhang.2016}}

  \begin{minipage}[c]{0.49\linewidth}
    Consider cable a \alert{1D continuum} placed in 3D with%
    %
    \begin{itemize}
      \item its shape given through its \alert{neutral axis}%
        %
        \vspace*{-0.50\baselineskip}%
        \begin{align*}%
          \curve \of{\lpathcoord, t}
            & \in \cablebody \subset \mathds{E}^{3} \,
              ,
            \\
          \lpathcoord \in \interval{0}{\cablen \of{t}}
            & \dotso \text{unstrained path coordinate} \,
              ,
            \\
          t \in \interval[open right]{0}{\infty}
            & \dotso \text{time; independent variable} \,
              ,
        \end{align*}
      \item an \alert{orthonormal director triad}~$\vectset{ \tangent ; \normal ; \binormal }$%
        \vspace*{-0.25\baselineskip}%
        \begin{align*}
          \tangent
            & = \frac{
                  \p{\curve}
                }{
                  \norm{\p{\curve}}
                } \,
                ,
            \\
          \normal
            & = \frac{
                  \p{\tangent}%
                }{
                  \norm{
                    \p{\tangent}
                  }
                }
              = \frac{
                  \crossp{
                    \p{\curve}
                  }{
                    \crossp*{
                      \pp{\curve}
                    }{
                      \p{\curve}
                    }
                  }
                }{
                  \norm{
                    \crossp{
                      \p{\curve}
                    }{
                      \crossp*{
                        \pp{\curve}
                      }{
                        \p{\curve}
                      }
                    }
                  }
                } \,
                ,
            \\
          \binormal
            & = \crossp{
                  \tangent
                }{
                  \normal
                }
              = \frac{
                  \crossp{
                    \p{\curve}
                  }{
                    \pp{\curve}
                  }
                }{
                  \norm{
                    \crossp{
                      \p{\curve}
                    }{
                      \pp{\curve}
                    }
                  }
                } \,
                ,
            \\
          \p{\parentheses{}}
            & = \pd{
                  \parentheses{}
                }{
                  \lpathcoord
                }
        \end{align*}
    \end{itemize}%
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      % \tikzexternalenable%
      % \tikzsetnextfilename{kinematics--coserat-rod--spatial}
      % \includegraphics[%
      %     width=0.95\linewidth,%
      %   ]{kinematics/cosserat-rod/spatial}
      \includegraphics[%
          % width=0.95\linewidth,%
          height=0.65\textheight,%
          axisratio=1.00,%
        ]{cable-with-tnb}
      \caption{1D cable continuum with orthonormal director triad at some point $\curve \of{\lpathcoord^{\ast}, t}$.}
      % \tikzexternaldisable
    \end{figure}
  \end{minipage}%
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Measurements of Deformation}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname: Frenet Serret Formulae~\citep{Frenet.1852,Arreaga.2001}}

  \begin{minipage}[c]{0.49\linewidth}
    \alert{Kinematic properties} of curve through%
    %
    \vspace*{-0.25\baselineskip}%
    \begin{align*}
      \pd{
        \tangent
      }{
        \lpathcoord
      }
        & = \curvature \,
            \normal \,
            ,
        \\
      \pd{
        \normal
      }{
        \lpathcoord
      }
        & = - \curvature \,
            \tangent
            +
            \torsion \,
            \binormal \,
            ,
        \\
      \pd{
        \binormal
      }{
        \lpathcoord
      }
        & = - \torsion \,
            \normal \,
            ,
    \end{align*}
    
    Formalism introduces measures on curve
    \begin{itemize}
      \item \alert{curvature}: failure to be a straight line%
        %
        \vspace*{-0.25\baselineskip}%
        \begin{align*}
          \curvature = \frac{ \norm{ \crossp{ \p{\curve} }{ \pp{\curve} } } }{ \pow[3]{ \norm{ \p{\curve} } } } \, ,
        \end{align*}
      \item \alert{torsion}: failure to be planar%
        %
        \vspace*{-0.25\baselineskip}%
        \begin{align*}
          \torsion = \frac{ \dotp{ \p{\curve} }{ \crossp*{ \pp{\curve} }{ \ppp{\curve} } } }{ \pow[2]{ \norm{ \crossp{ \p{\curve} }{ \pp{\curve} } } } } \, ,
        \end{align*}
    \end{itemize}
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      \input{animations/frenetserret/torusknot}
    \end{figure}
  \end{minipage}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Discretization}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname~\citep{Lang.2011,Cao.2008}}

  \begin{minipage}[c]{0.49\linewidth}
    Discrete form of continuum formulation of curve needed for numerical integration%
    %
    \begin{itemize}
      \item Describe shape through \alert{discrete set} of displacement functions%
        %
        \vspace*{-0.50\baselineskip}%
        \begin{align*}
          \curve \of{\lpathcoord, t}
            & = \shapefuns \of{\lpathcoord} \,
                \genq \of{t}
              = \begin{bmatrix}
                \shapefuns_{\ms{x}} \of{\lpathcoord} \,
                  \genq_{\ms{x}} \of{t}
                  \\
                \shapefuns_{\ms{y}} \of{\lpathcoord} \,
                  \genq_{\ms{y}} \of{t}
                  \\
                \shapefuns_{\ms{z}} \of{\lpathcoord} \,
                  \genq_{\ms{z}} \of{t}
                \end{bmatrix}
        \end{align*}
      \item \alert{B-Splines} form components of $\shapefuns \of{\lpathcoord}$%
        \begin{itemize}
          \item Piecewise polynomial function of degree~$\polydeg$
          \item Up to~$\polydeg$ continuous derivatives
          \item Shape defined through control points (consider them $\genq \of{t}$)
        \end{itemize}
    \end{itemize}
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      % \tikzexternalenable%
      % \tikzsetnextfilename{bezier-curve}
      \includegraphics[%
          width=0.75\linewidth,
          height=0.35\textheight,%
          % axisratio=1.00,%
        ]{b-spline/shape}
      % \vspace*{-0.50\baselineskip}
      % \caption{Planar curve confined by convex hull of its control points.}
      % \tikzexternaldisable
    \end{figure}%
    \vspace*{-1.50\baselineskip}%
    \begin{figure}
      \centering
      \smaller[2]
      % \tikzexternalenable%
      % \tikzsetnextfilename{bezier-curve}
      \includegraphics[%
          width=\linewidth,
          height=0.30\textheight,%
          % axisratio=3.00,%
        ]{b-spline/bases}%
      \vspace*{-1.00\baselineskip}%
      \caption{B-Spline bases functions for above planar curve.}
      % \tikzexternaldisable
    \end{figure}%
  \end{minipage}%
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Constrained Dynamics Formulation}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{minipage}[c]{0.49\linewidth}
    D'Alembert's principle of \alert{virtual work}%
    %
    \vspace*{-0.25\baselineskip}%
    \begin{align*}
      \virtualwork
        & = \sum\limits_{i = 1}^{N}{
              \transpose{
                \parentheses*{
                  \vect{f}_{i}
                  -
                  \mass_{i} \,
                  \od[2]{
                    \vect{x}_{i}
                  }{
                    t
                  }
                }
              } \,
              \virtualdisplacement_{i}
            }
          = 0 \,
            ,
    \end{align*}
    
    provides \alert{force formulation}%
    %
    \vspace*{-0.25\baselineskip}%
    \begin{align*}
      \zeros
        & = \transpose*{
              \vect{f}_{\msdynamic}
              +
              \vect{f}_{\msinternal}
              +
              \vect{f}_{\msexternal}
            } \,
            \virtualdisplacement \,
            .
    \end{align*}
    
    Reading in \alert{generalized coordinates} $\genq$%
    %
    \vspace*{-0.25\baselineskip}%
    \begin{align*}
      \zeros
        & = \Mass \,
            \ddotgenq \of{t}
            +
            \vect{f}_{\msinternal} \of{ \genq }
            +
            \vect{f}_{\msexternal} \of{ \genq, \dotgenq }
            \,
            ,
        \\
      \zeros
        & = \posconstraints \of{ \genq, t } \,
            ,
    \end{align*}
    
    \alert{Constrain} cable proximal and distal points
    %
    \begin{align*}
      \zeros
        & = \curve \of{\lpathcoord = 0, t}
            -
            \linpos_{\subproximal} \of{t}
          = \posconstraints_{\subproximal} \of{ \genq, t }
            \,
            ,
          \\
      \zeros
        & = \curve \of{\lpathcoord = \cablen, t}
            -
            \linpos_{\subdistal} \of{t}
          = \posconstraints_{\subdistal} \of{ \genq, t }
            \,
            .
    \end{align*}
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    % In addition, cable proximal and distal points are constrained
    % %
    % \begin{align*}
    %   \zeros
    %     & = \curve \of{\pathcoord = 0, t}
    %         -
    %         \linpos_{\subproximal \of{t}}
    %       = \posconstraints_{\subproximal} \of{ \genq, t }
    %         \,
    %         ,
    %       \\
    %   \zeros
    %     & = \curve \of{\pathcoord = \cablen, t}
    %         -
    %         \linpos_{\subdistal} \of{t}
    %       = \posconstraints_{\subdistal} \of{ \genq, t }
    %         \,
    %         .
    % \end{align*}

    \begin{figure}
      \centering
      \smaller[2]
      \input{animations/cable/nurbs2/distalmotion}
    \end{figure}
  \end{minipage}%
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Time-Varying Length}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname~\citep{Zhu.2005}}

  \begin{minipage}[c]{0.49\linewidth}
    Forces are expressed as integral formulation over continuum i.e., expressed over \alert{unstrained length}%
    \begin{align*}
      \vect{f}_{\parentheses{}}
        & = \int\limits_{0}^{\cablen}{
                f \of{\lpathcoord} \,
                \td{\lpathcoord}
              } \,
              ,
    \end{align*}
    
    New \alert{unitary} curvilinear abscissae~${\pathcoord = \lpathcoord / \cablen}$ allowing for \alert{integration by substitution}%
    \begin{align*}
      \int\limits_{ \varphi \of{a} }^{ \varphi \of{b} }{
        f \of{u} \,
        \td{u}
      }
        & = \int\limits_{a}^{b}{
            f \of{ \varphi \of{x} } \,
            \varphi^{ \prime } \of{x} \,
            \td{x}
          } \,
          ,
    \end{align*}
    
    yields%
    \vspace*{-0.50\baselineskip}%
    \begin{align*}
      \vect{f}_{\parentheses{}}
        & = \int\limits_{0}^{1}{
              f \of{\pathcoord} \,
              \cablen \,
              \td{\pathcoord}
            } \,
            ,
    \end{align*}
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      \input{animations/cable/nurbs2/lengthchange}
    \end{figure}
  \end{minipage}%
\end{frame}
