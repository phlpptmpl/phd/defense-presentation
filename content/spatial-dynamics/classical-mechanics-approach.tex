%!TEX root=../../source.tex
%!TEX file=content/spatial-dynamics/classical-mechanics-approach.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Classical Mechanics Approach}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\subsecname}
  \framesubtitle{Assumptions \& Steps to Take}
  
  \begin{minipage}[t]{0.49\linewidth}
    \alert{Assumptions:} Cable \dotso
    
    % Cable \dotso%
    \begin{itemize}
      \item can undergo large deformations
      \item deforms like Hookean solid: $\stress* = \elasticity* \, \strain*$
      \item interacts only with platform, no additional interaction
      \item not coiled or guided 
    \end{itemize}
  \end{minipage}%
  \hfill%
  \begin{minipage}[t]{0.49\linewidth}
    \alert{Steps to take}
    
    \begin{enumerate}
      \item Geometric description of cable
      \item Measurement of deformation
      \item Discretization of continuum
      \item Time-varying length
    \end{enumerate}
  \end{minipage}%
  \hfill%
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\subsecname}
  \framesubtitle{Geometric Description of Curve: Cosserat Rod~\citep{Soler.2018,Cao.2008}}

  \begin{minipage}[c]{0.49\linewidth}
    Consider cable a 1D continuum placed in 3D with%
    %
    \begin{itemize}
      \item its shape given through its neutral axis reading%
        %
        \vspace*{-0.50\baselineskip}%
        \begin{align*}%
          \curve \of{\lpathcoord, t}
            & \in \cablebody \subset \mathds{E}^{3} \,
              ,
            \\
          \lpathcoord \in \interval{0}{\cablen}
            & \dotso \text{unstrained path coordinate} \,
              ,
            \\
          t \in \interval[open right]{0}{\infty}
            & \dotso \text{time; independent variable} \,
              ,
        \end{align*}
      \item an orthonormal director triad~$\vectset{ \tangent ; \normal ; \binormal }$%
        \vspace*{-0.25\baselineskip}%
        \begin{align*}
          \tangent
            & = \frac{
                  \p{\curve}
                }{
                  \norm{\p{\curve}}
                } \,
                ,
            \\
          \normal
            & = \frac{
                  \p{\tangent}%
                }{
                  \norm{
                    \p{\tangent}
                  }
                }
              = \frac{
                  \crossp{
                    \p{\curve}
                  }{
                    \crossp*{
                      \pp{\curve}
                    }{
                      \p{\curve}
                    }
                  }
                }{
                  \norm{
                    \crossp{
                      \p{\curve}
                    }{
                      \crossp*{
                        \pp{\curve}
                      }{
                        \p{\curve}
                      }
                    }
                  }
                } \,
                ,
            \\
          \binormal
            & = \crossp{
                  \tangent
                }{
                  \normal
                }
              = \frac{
                  \crossp{
                    \p{\curve}
                  }{
                    \pp{\curve}
                  }
                }{
                  \norm{
                    \crossp{
                      \p{\curve}
                    }{
                      \pp{\curve}
                    }
                  }
                } \,
                ,
            \\
          \p{\parentheses{}}
            & = \pd{
                  \parentheses{}
                }{
                  \lpathcoord
                }
        \end{align*}
    \end{itemize}%
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      % \tikzexternalenable%
      % \tikzsetnextfilename{kinematics--coserat-rod--spatial}
      \includegraphics[%
          width=0.95\linewidth,%
        ]{kinematics/cosserat-rod/spatial}
      % \tikzexternaldisable
    \end{figure}
  \end{minipage}%
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\subsecname}
  \framesubtitle{Measurements of Deformation: Frenet Serret Formulae~\citep{Frenet.1852,Arreaga.2001}}

  \begin{minipage}[c]{0.49\linewidth}
    \alert{Kinematic properties} of curve through%
    %
    \vspace*{-0.25\baselineskip}%
    \begin{align*}
      \pd{
        \tangent
      }{
        \lpathcoord
      }
        & = \curvature \,
            \normal \,
            ,
        \\
      \pd{
        \normal
      }{
        \lpathcoord
      }
        & = - \curvature \,
            \tangent
            +
            \torsion \,
            \binormal \,
            ,
        \\
      \pd{
        \binormal
      }{
        \lpathcoord
      }
        & = - \torsion \,
            \normal \,
            ,
    \end{align*}
    
    Formalism introduces measures on curve
    \begin{itemize}
      \item \alert{curvature}: failure to be a straight line%
        %
        \vspace*{-0.25\baselineskip}%
        \begin{align*}
          \curvature = \frac{ \norm{ \crossp{ \p{\curve} }{ \pp{\curve} } } }{ \pow[3]{ \norm{ \p{\curve} } } } \, ,
        \end{align*}
      \item \alert{torsion}: failure to be planar%
        %
        \vspace*{-0.25\baselineskip}%
        \begin{align*}
          \torsion = \frac{ \dotp{ \p{\curve} }{ \crossp*{ \pp{\curve} }{ \ppp{\curve} } } }{ \pow[2]{ \norm{ \crossp{ \p{\curve} }{ \pp{\curve} } } } } \, ,
        \end{align*}
    \end{itemize}
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      \input{animations/frenetserret/torusknot}
    \end{figure}
  \end{minipage}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\subsecname}
  \framesubtitle{Discretization~\citep{Lang.2011,Soler.2018,Zhang.2016}}

  \begin{minipage}[c]{0.49\linewidth}
    Discrete form of continuum formulation of cable needed for numerical integration%
    %
    \begin{itemize}
      \item Describe shape through \alert{discrete set} of displacement functions%
        %
        \vspace*{-0.50\baselineskip}%
        \begin{align*}
          \curve \of{\lpathcoord, t}
            & = \shapefuns \of{\lpathcoord} \,
                \genq \of{t}
              = \begin{bmatrix}
                \shapefuns_{\ms{x}} \of{\lpathcoord} \,
                  \genq_{\ms{x}} \of{t}
                  \\
                \shapefuns_{\ms{y}} \of{\lpathcoord} \,
                  \genq_{\ms{y}} \of{t}
                  \\
                \shapefuns_{\ms{z}} \of{\lpathcoord} \,
                  \genq_{\ms{z}} \of{t}
                \end{bmatrix}
        \end{align*}
      \item \alert{B-Splines} form components of $\shapefuns \of{\lpathcoord}$%
        \begin{itemize}
          \item Piecewise polynomial function of degree~$\polydeg$
          \item Up to~$\polydeg$ continuous derivatives
          \item Shape defined through control points (consider them $\genq \of{t}$)
        \end{itemize}
    \end{itemize}
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      % \tikzexternalenable%
      % \tikzsetnextfilename{bezier-curve}
      \includegraphics[%
          width=\linewidth,
          height=0.40\textheight,%
          % axisratio=3.00,%
        ]{b-spline/shape}
      % \tikzexternaldisable
    \end{figure}%
    \vspace*{-1.00\baselineskip}%
    \begin{figure}
      \centering
      \smaller[2]
      % \tikzexternalenable%
      % \tikzsetnextfilename{bezier-curve}
      \includegraphics[%
          width=\linewidth,
          height=0.40\textheight,%
          % axisratio=3.00,%
        ]{b-spline/bases}
      % \tikzexternaldisable
    \end{figure}%
  \end{minipage}%
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\subsecname}
  \framesubtitle{Constrained Dynamics Formulation}

  \begin{minipage}[c]{0.49\linewidth}
    D'Alembert's principle of \alert{virtual work}%
    %
    \vspace*{-0.25\baselineskip}%
    \begin{align*}
      \virtualwork
        & = \sum\limits_{i = 1}^{N}{
              \transpose{
                \parentheses*{
                  \vect{f}_{i}
                  -
                  \mass_{i} \,
                  \od[2]{
                    \vect{x}_{i}
                  }{
                    t
                  }
                }
              } \,
              \virtualdisplacement_{i}
            }
          = 0 \,
            ,
    \end{align*}
    
    provides \alert{force formulation}%
    %
    \vspace*{-0.25\baselineskip}%
    \begin{align*}
      \zeros
        & = \transpose*{
              \vect{f}_{\msinternal}
              -
              \vect{f}_{\msexternal}
              +
              \vect{f}_{\msdynamic}
            } \,
            \virtualdisplacement \,
            .
    \end{align*}
    
    Reading in \alert{generalized coordinates} $\genq$%
    %
    \vspace*{-0.25\baselineskip}%
    \begin{align*}
      \zeros
        & = \Mass \,
            \ddotgenq \of{t}
            -
            \vect{f}_{\msexternal} \of{ \genq, \dotgenq \of{t} }
            +
            \vect{f}_{\msinternal} \of{ \genq }
            \,
            ,
        \\
      \zeros
        & = \posconstraints \of{ \genq, t } \,
            ,
    \end{align*}
    
    \alert{Constrain} cable proximal and distal points
    %
    \begin{align*}
      \zeros
        & = \curve \of{\pathcoord = 0, t}
            -
            \linpos_{\subproximal \of{t}}
          = \posconstraints_{\subproximal} \of{ \genq, t }
            \,
            ,
          \\
      \zeros
        & = \curve \of{\pathcoord = 1, t}
            -
            \linpos_{\subdistal} \of{t}
          = \posconstraints_{\subdistal} \of{ \genq, t }
            \,
            .
    \end{align*}
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    % In addition, cable proximal and distal points are constrained
    % %
    % \begin{align*}
    %   \zeros
    %     & = \curve \of{\pathcoord = 0, t}
    %         -
    %         \linpos_{\subproximal \of{t}}
    %       = \posconstraints_{\subproximal} \of{ \genq, t }
    %         \,
    %         ,
    %       \\
    %   \zeros
    %     & = \curve \of{\pathcoord = \cablen, t}
    %         -
    %         \linpos_{\subdistal} \of{t}
    %       = \posconstraints_{\subdistal} \of{ \genq, t }
    %         \,
    %         .
    % \end{align*}

    \begin{figure}
      \centering
      \smaller[2]
      \input{animations/cable/nurbs2/distalmotion}
    \end{figure}
  \end{minipage}%
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{\subsecname}
  \framesubtitle{Time-Varying Length~\citep{Zhu.2005}}

  \begin{minipage}[c]{0.49\linewidth}
    Forces are expressed as integral formulation over continuum
    
    Expressed over \alert{unstrained length}%
    \begin{align*}
      \vect{f}_{\parentheses{}}
        & = \int\limits_{0}^{\cablen}{
                f \of{\lpathcoord} \,
                \td{\lpathcoord}
              } \,
              ,
    \end{align*}
    
    New \alert{unitary} curvilinear abscissae~${\pathcoord = \lpathcoord / \cablen}$ %with $\td{\lpathcoord} = \cablen \td{\pathcoord}$
    
    \alert{Integration by substitution}%
    \begin{align*}
      \int\limits_{ \varphi \of{a} }^{ \varphi \of{b} }{
        f \of{u} \,
        \td{u}
      }
        & = \int\limits_{a}^{b}{
            f \of{ \varphi \of{x} } \,
            \varphi^{ \prime } \of{x} \,
            \td{x}
          } \,
          ,
    \end{align*}
    
    yields%
    \vspace*{-0.50\baselineskip}%
    \begin{align*}
      \vect{f}_{\parentheses{}}
        & = \int\limits_{0}^{1}{
              f \of{\pathcoord} \,
              \cablen \,
              \td{\pathcoord}
            } \,
            ,
    \end{align*}
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      \input{animations/cable/nurbs2/lengthchange}
    \end{figure}
  \end{minipage}%
\end{frame}
