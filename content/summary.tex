%!TEX root=../source.tex
%!TEX file=content/summary.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Closing}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{}
\begin{frame}[standout]
  Q: How to (re-)incorporate cables into cable robot description to consider both their motion and their interaction with the platform?
  
  \pause
  
  A: Consider cables to be slender Cosserat rods and compose a multibody simulation framework.
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Summary}
  
  \begin{minipage}[c]{0.49\linewidth}
    Evaluated \alert{cable modeling} approaches found in literature
    \begin{itemize}
      \item Linear wave equation
      \item Rigid finite element method
      \item \alert{Cosserat rod} based on elastica theory
    \end{itemize}
    
    Derived a \alert{multibody framework} for simulation of cable robots
    \begin{itemize}
      \item Solving of DAE using \alert{mechanical integrator}
      \item Iterative integration scheme using \alert{Newton-Raphson} root-finding
      \item \alert{Consistency} with existing cable robot analysis methods and algorithms shown
    \end{itemize}
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      \includegraphics[%
          width=1.00\linewidth,%
          axisratio=1.00,%
        ]{caro-dynamics/named/ipanema-mini/statics/shape}
    \end{figure}
  \end{minipage}%
  
  \begin{takeawayblock}{}
    First multibody simulation framework for cable robots with a classical mechanics-based cable model and consideration of numerical integration
  \end{takeawayblock}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{Outlook}
  
  \begin{minipage}[c]{0.49\linewidth}
    \begin{itemize}
      \item Incorporate damping (cf.\@~\citet{Lang.2013,Spillmann.2007})
      \item Assess different mechanical integrators e.g., projective dynamics
      \item Experimental validation of simulation model
    \end{itemize}
  \end{minipage}%
  \hfill%
  \begin{minipage}[c]{0.49\linewidth}
    \begin{figure}
      \centering
      \smaller[2]
      \includegraphics[%
          width=1.00\linewidth,%
          axisratio=1.00,%
        ]{caro-dynamics/named/expo/statics/shape}
    \end{figure}
  \end{minipage}%
\end{frame}
