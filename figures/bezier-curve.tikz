%!TEX root=../thesis.tex
%!TEX file=figures/bezier-curve.tikz

\begin{tikzpicture}[
  ctrlpoint/.style={%
    draw=gray,%
    circle,%
    inner sep=0,%
    minimum width=0.05cm,%
  },%
]

% Control points
\node[
  ctrlpoint,%
]
  (p0)
  at (0,0)
  {};
\node[
  ctrlpoint,%
]
  (p1)
  at (1,1)
  {};
\node[
  ctrlpoint,%
]
  (p2)
  at (4,-1)
  {};
\node[
  ctrlpoint,%
]
  (p3)
  at (4,0.5)
  {};

% Labels of control points
\node[%
    anchor=east,%
  ]%
  (p0-label)%
    at (p0)%
    {%
      $\ctrlpoint_{0}$%
    };
\node[%
    anchor=south,%
  ]%
  (p1-label)%
    at (p1)%
    {%
      $\ctrlpoint_{1}$%
    };
\node[%
    anchor=north,%
  ]%
  (p2-label)%
    at (p2)%
    {%
      $\ctrlpoint_{2}$%
    };
\node[%
    anchor=west,%
  ]%
  (p3-label)%
    at (p3)%
    {%
      $\ctrlpoint_{3}$%
    };

% Lines connecting the control points
\draw[
    gray,%
  ]%
  (p0)%
  -- (p1)%
  -- (p2)%
  -- (p3);

% Conex hull of control points
\draw[%
    dashed,%
  ]%
  (p0)%
  -- (p1)%
  -- (p3)%
  -- (p2)%
  -- (p0)%
  -- cycle;

% Bezier curve
\draw[%
    double=Blue,%
    draw=White,%
    semithick,%
    double distance=2.00\pgflinewidth,%
  ]%
  (p0)%
  .. controls (p1) and (p2)%
  .. (p3);

\end{tikzpicture}
