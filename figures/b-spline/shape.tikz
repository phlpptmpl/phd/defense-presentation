%!TEX root=../../../../../thesis.tex
%!TEX file=figures/caro-dynamics/named/ipanema-3/motion_up/platform.tikz

%\setlength{\subplothsep}{1.00\baselineskip}
%\setlength{\subplotvsep}{2.50\baselineskip}
\setlength{\subplotwidth}{1.00\linewidth/2 - 2\subplothsep}
\setlength{\subplotheight}{1.00\subplotwidth}

\begin{tikzpicture}[%
  ]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% platform position
\begin{axis}[
    % General adjustments
    line plot,%
    tufte-like,%
    cycle multiindex* list={%
      /ustutt/linestyles*\nextlist%
      /ustutt/color list*%
    },%
    %
    % Legend
    %
    % Axis
    axis lines=none,%
    % 
    % X-Axis
    xmin=0.00,%
    xmax=4.00,%
    enlarge x limits=0.05,%
    xlabel={Abscissae $\pathcoord$},%
    xlabel near ticks,%
    % 
    % Y-Axis
    ymin=-1.00,%
    ymax=1.00,%
    enlarge y limits=0.05,%
    ylabel={Value},%
    ylabel near ticks,%
    %
    % Additional stuff
    labeled/.style args={#1}{%
      nodes near coords,%
      every node near coord/.style={%
        anchor=\myanchor,%
      },%
    },%
  ]

% Shape
\addplot+[%
    /ustutt/semithick,%
  ] table[%
      col sep=comma,%
      x=x,%
      y=z,%
    ]{data/b-spline/shape.dat};

% Control points themselves
\addplot+[%
    /ustutt/semithick,%
    visualization depends on={value \thisrow{label} \as \mylabel},
    visualization depends on={value \thisrow{anchor} \as \myanchor},
    nodes near coords*={$\mylabel$},%
    every node near coord/.append style={%
      anchor=\myanchor,%
    },%
  ]table[%
      col sep=comma,%
      x=x,%
      y=z,%
    ]{data/b-spline/controlpoints.dat};

% Bounding box of control points
\addplot+[%
    /ustutt/semithick,%
    no markers,%
  ]table[%
      col sep=comma,%
      x=x,%
      y=z,%
    ]{data/b-spline/convexhull.dat};

\end{axis}

\end{tikzpicture}
