%!TEX root=../../../../../thesis.tex
%!TEX file=figures/caro-dynamics/named/ipanema-3/motion_up/energies.tikz

%\setlength{\subplothsep}{1.00\baselineskip}
%\setlength{\subplotvsep}{2.50\baselineskip}
\setlength{\subplotwidth}{1.00\linewidth - 7.25\baselineskip}
\setlength{\subplotheight}{0.30\subplotwidth}

\pgfplotstableread[%
  col sep=comma,%
]{data/cdpr/named/motion_up/ipanema_3/degree-7/segment-11/energies.kinetic.dat}\plotdataKinetic

\pgfplotstableread[%
  col sep=comma,%
]{data/cdpr/named/motion_up/ipanema_3/degree-7/segment-11/energies.potential.dat}\plotdataPotential


\begin{tikzpicture}[
  ]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% cables
\begin{groupplot}[%
    group style={%
      % General stuff
      group name={caro dynamics ipanema-3 motion-up energies cables},%
      % Layout
      columns=1,%
      rows=2,%
      horizontal sep=\subplothsep,%
      vertical sep=\subplotvsep,%
      % X-Axes
      xlabels at=edge bottom,%
      xticklabels at=edge bottom,%
      % Y-Axes
      ylabels at=all,%
      yticklabels at=edge left,%
    },%
    %
    % General adjustments
    timeseries plot,%
    yyaxis left,%
    cycle multiindex* list={%
      /phlpptmpl/linestyles*\nextlist%
      /phlpptmpl/mark list even*%
    },%
    width=\subplotwidth,%
    height=\subplotheight,%
    %
    % X-Axis
    xmin=1.00,%
    xmax=5.00,%
    %
    % Y-Axis
    ymin=0,%
  ]



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% kinetic energies
\nextgroupplot[%
    % General adjustments
    title={Kinetic energies},%
    % 
    % X-Axis
    % 
    % Y-Axis
    ylabel={Energy $\energykinetic_{\subcable, \cabindex} / \si{\joule}$},
    ymax=0.10,%
    scaled y ticks=base 10:2,%
  ]

\foreach \cid in {1,2,3,4,5,6,7,8}{%
  \addplot+[%
    ]table[%
        col sep=comma,%
        x=t,%
        y=c\cid,%
      ]{\plotdataKinetic};
    \expandafter\label\expandafter{plt:caro-dynamics:dynamics:ipanema-3:motion-up:energies:c\cid}
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% potential energies
\nextgroupplot[%
    % General adjustments
    title={Potential energies},%
    % 
    % X-Axis
    % 
    % Y-Axis
    ylabel={Energy $\energypotential_{\subcable, \cabindex} / \si{\joule}$},
    ymax=40,%
  ]

\foreach \cid in {1,2,3,4,5,6,7,8}{%
  \addplot+[%
    ]table[%
        col sep=comma,%
        x=t,%
        y=c\cid,%
      ]{\plotdataPotential};
}

\end{groupplot}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% platform
\begin{groupplot}[%
    group style={%
      % General stuff
      group name={caro dynamics ipanema-3 motion-up energies platform},%
      % Layout
      columns=1,%
      rows=2,%
      horizontal sep=\subplothsep,%
      vertical sep=\subplotvsep,%
      % X-Axes
      xlabels at=edge bottom,%
      xticklabels at=edge bottom,%
      % Y-Axes
      ylabels at=all,%
      yticklabels at=edge left,%
    },%
    %
    % General adjustments
    timeseries plot,%
    cycle multiindex* list={%
      /phlpptmpl/linestyles*\nextlist%
      /phlpptmpl/mark list even*%
    },%
    yyaxis right,%
    width=\subplotwidth,%
    height=\subplotheight,%
    %
    % X-Axis
    xmin=1.00,%
    xmax=5.00,%
    %
    % Y-Axis
    ymin=0,%
  ]



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% kinetic energies
\nextgroupplot[%
    % General adjustments
    title={Kinetic energies},%
    %
    % Legend
    legend to name={caro dynamics ipanema-3 motion-up energies cables legend},%
    legend style={%
      legend columns=5,%
      /tikz/every even column/.append style={%
        column sep=1.00ex,%
      },%
      % transpose legend,%
    },%
    % 
    % X-Axis
    % 
    % Y-Axis
    ylabel={Energy $\energykinetic_{\subplatform} / \si{\joule}$},
    ymajorgrids=false,%
    ymax=100,%
  ]

% Shift cycle index to account for cable styles
\pgfplotsset{cycle list shift=8}

% Add platforms
\foreach \pid in {1}{%
  \addplot+[%
    ]table[%
        col sep=comma,%
        x=t,%
        y=p\pid,%
      ]{\plotdataKinetic};
  \addlegendentry{Platform}
}

\addlegendimage{refstyle={plt:caro-dynamics:dynamics:ipanema-3:motion-up:energies:c1}}
\addlegendentry{$\cabindex = 1$}
\addlegendimage{refstyle={plt:caro-dynamics:dynamics:ipanema-3:motion-up:energies:c2}}
\addlegendentry{$\cabindex = 2$}
\addlegendimage{refstyle={plt:caro-dynamics:dynamics:ipanema-3:motion-up:energies:c3}}
\addlegendentry{$\cabindex = 3$}
\addlegendimage{refstyle={plt:caro-dynamics:dynamics:ipanema-3:motion-up:energies:c4}}
\addlegendentry{$\cabindex = 4$}
\addlegendimage{empty legend}
\addlegendentry{}
\addlegendimage{refstyle={plt:caro-dynamics:dynamics:ipanema-3:motion-up:energies:c5}}
\addlegendentry{$\cabindex = 5$}
\addlegendimage{refstyle={plt:caro-dynamics:dynamics:ipanema-3:motion-up:energies:c6}}
\addlegendentry{$\cabindex = 6$}
\addlegendimage{refstyle={plt:caro-dynamics:dynamics:ipanema-3:motion-up:energies:c7}}
\addlegendentry{$\cabindex = 7$}
\addlegendimage{refstyle={plt:caro-dynamics:dynamics:ipanema-3:motion-up:energies:c8}}
\addlegendentry{$\cabindex = 8$}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% potential energies
\nextgroupplot[%
    % General adjustments
    title={Potential energies},%
    % 
    % X-Axis
    % 
    % Y-Axis
    ylabel={Energy $\energypotential_{\subplatform} / \si{\joule}$},
    ymajorgrids=false,%
    ymax=200,%
  ]

% Shift cycle index to account for cable styles
\pgfplotsset{cycle list shift=8}

\foreach \pid in {1}{%
  \addplot+[%
    ]table[%
        col sep=comma,%
        x=t,%
        y=p\pid,%
      ]{\plotdataPotential};
}

\end{groupplot}

\node[%
    yshift=-1.50\subplotvsep,%
  ]%
  at (caro dynamics ipanema-3 motion-up energies cables c1r2.south)%
  {%
    \ref{caro dynamics ipanema-3 motion-up energies cables legend}%
  };

\end{tikzpicture}
