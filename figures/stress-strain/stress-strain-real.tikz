%!TEX root=../../source.tex
%!TEX file=figures/stress-strain/stress-strain-real.tikz

\pgfplotstableread[%
    col sep=comma,%
  ]{data/stress-strain/example-dpro/liros-dpro.dat}\experimentaltable
\pgfplotstableread[%
    col sep=comma,%
  ]{data/stress-strain/example-dpro/hookean.dat}\modeltable

\begin{tikzpicture}[%
    /ustutt/semithick,%
  ]

\begin{axis}[%
    % General adjustments
    line plot,%
    tufte-like,%
    name={stress-strain stress-strain-real},%
    cycle multiindex* list={%
      % /ustutt/mark list even*\nextlist%
      /ustutt/color list*
    },%
    %
    % Legend
    % legend to name={stress-strain stress-strain-real legend},%
    legend style={%
      at={(0.50,1.05)},%
      anchor=south,%
      legend columns=2,%
      /tikz/every even column/.append style={%
        column sep=1.00ex,%
      },%
    },%
    %
    % X-Axis
    xmin=0.00,%
    xmax=3.50,%
    enlarge x limits=0.05,%
    xlabel={Elongation $\delcablen / \si{\milli\meter}$},%
    % 
    % Y-axis
    ymin=0,%
    ymax=400,%
    enlarge y limits=0.05,%
    ylabel={Cable force $\cabforce / \si{\newton}$},%
  ]


\tikzustuttset{marker count=13}
\addplot+[%
    /ustutt/semithick,%
    % mark repeat=689,%
  ]table[%
    x=elong,%
    y=force,%
    col sep=comma,%
  ]%
  {\experimentaltable};
\addlegendentry{\Dyneema{} SK78}

\tikzustuttset{marker count=7}
\addplot+[%
    /ustutt/semithick,%
    % mark repeat=689,%
  ]table[%
    x=elong,%
    y=force,%
    col sep=comma,%
  ]%
  {\modeltable};
\addlegendentry{Hookean' Cable}

% Nodes for tensing direction annotation
\node[%
    coordinate,%
  ]%
  at (axis cs:0.00,50)%
  (tense start)%
  {};
\node[%
    coordinate,%
  ]%
  at (axis cs:3.00,375)%
  (tense end)%
  {};

% Nodes for relaxation direction annotation
\node[%
    coordinate,%
  ]%
  at (axis cs:3.50,350)%
  (relax start)%
  {};
\node[%
    coordinate,%
  ]%
  at (axis cs:1.00,25)%
  (relax end)%
  {};

\end{axis}

% \node[%
%     yshift=-2.50\baselineskip,%
%     anchor=north,%
%   ]%
%   at (stress-strain stress-strain-real.south)%
%   {%
%     \ref{stress-strain stress-strain-real legend}%
%   };

% Line showing in which direction we tense and relax
\draw[%
    ->,%
  ]%
  (tense start)%
  -- (tense end)%
    node[%
        pos=0.50,%
        anchor=south,%
        sloped,%
      ]%
      {tense};
\draw[%
    ->,%
  ]%
  (relax start)%
  to[%
      out=245,%
      in=15,%
      looseness=0.75,%
    ]%
    node[%
        pos=0.50,%
        anchor=north,%
        sloped,%
      ]%
      {relax}%
    (relax end);

\end{tikzpicture}%
