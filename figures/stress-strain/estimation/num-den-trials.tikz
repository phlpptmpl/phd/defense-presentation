%!TEX root=../../../thesis.tex
%!TEX file=figures/stress-strain/estimation/num-den-trials.tikz

%\setlength{\subplothsep}{1.00\baselineskip}
%\setlength{\subplotvsep}{2.50\baselineskip}
\setlength{\subplotwidth}{0.75\linewidth/2}% - \subplothsep}
\setlength{\subplotheight}{1.00\subplotwidth} % make it square

\pgfmathsetmacro{\myboxplotboxextend}{0.20}

\begin{tikzpicture}

\begin{groupplot}[%
    group style={
      % General stuff
      group name=stress strain estimation num den trials,%
      % Layout
      columns=2,%
      rows=1,%
      horizontal sep=\subplothsep,%
      vertical sep=\subplotvsep,%
      % X-Axes
      xlabels at=edge bottom,%
      xticklabels at=edge bottom,%
      % Y-Axes
      ylabels at=edge left,%
      yticklabels at=all,%
    },%
    % General adjustments
    boxplot plot,%
    width=\subplotwidth,%
    height=\subplotheight,%
    %
    % Legend
    legend style={%
      /tikz/every even column/.append style={%
        column sep=1.00ex,%
      },%
      /tikz/every odd column/.append style={%
        column sep=0.25ex,%
      },%
      legend columns=-1,%
    },%
    %
    % Boxplot adjustments
    boxplot={
      draw direction=y,%
      box extend=\myboxplotboxextend,%
    },%
    %
    % X-Axis
    xmin=1,%
    xmax=4,%
    xtick={1,2,3,4},%
    enlarge x limits=0.05,%
    %
    % Y-Axis
    % ymin=1e-8,%
    % ytick scale label code/.code={},%
    ylabel={Numeric value},%
  ]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\nextgroupplot[%
    % General adjustments
    title={Numerator coefficients $\vect{b}_{\iterindex}$},%
    %
    % Legend
    legend to name={stress strain estimation num den trials grouplegend},%
    %
    % X-Axis
    xticklabels={$\vect{b}_{0}$,$\vect{b}_{1}$,$\vect{b}_{2}$,$\vect{b}_{3}$},%
    %
    % Y-Axis
    ymin=5*1e7,%
    ymax=1*1e11,%
    % ylabel={Numeric value},%
    ymode=log,%
    ymajorgrids=true,%
  ]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% N = 2
% E_1
\addplot+[%
    boxplot,%
    boxplot/draw position=1-1.20*\myboxplotboxextend,%
    area legend,%
    forget plot,%
  ] table[%
      y=CN0,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n2_trials_en.dat};
% E_1
\addplot+[%
    boxplot,%
    boxplot/draw position=2-1.20*\myboxplotboxextend,%
    area legend,%
  ] table[%
      y=CN1,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n2_trials_en.dat};
\addlegendentry{$n_{\msmaxwell} = 2$}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% N = 3
% E_1
\addplot+[%
    boxplot,%
    boxplot/draw position=1,%
    area legend,%
    forget plot,%
  ] table[%
      y=CN0,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n3_trials_en.dat};
% E_2
\addplot+[%
    boxplot,%
    boxplot/draw position=2,%
    area legend,%
    forget plot,%
  ] table[%
      y=CN1,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n3_trials_en.dat};
% E_3
\addplot+[%
    boxplot,%
    boxplot/draw position=3,%
    area legend,%
  ] table[%
      y=CN2,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n3_trials_en.dat};
\addlegendentry{$n_{\msmaxwell} = 3$}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% N = 4
% E_1
\addplot+[%
    boxplot,%
    boxplot/draw position=1+1.20*\myboxplotboxextend,%
    area legend,%
    forget plot,%
  ] table[%
      y=CN0,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n4_trials_en.dat};
% E_2
\addplot+[%
    boxplot,%
    boxplot/draw position=2+1.20*\myboxplotboxextend,%
    area legend,%
    forget plot,%
  ] table[%
      y=CN1,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n4_trials_en.dat};
% E_3
\addplot+[%
    boxplot,%
    boxplot/draw position=3+1.20*\myboxplotboxextend,%
    area legend,%
    forget plot,%
  ] table[%
      y=CN2,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n4_trials_en.dat};
% E_4
\addplot+[%
    boxplot,%
    boxplot/draw position=4+1.20*\myboxplotboxextend,%
    area legend,%
  ] table[%
      y=CN3,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n4_trials_en.dat};
\addlegendentry{$n_{\msmaxwell} = 4$}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\nextgroupplot[%
    % General adjustments
    title={Denominator coefficients $\vect{a}_{\iterindex}$},%
    %
    % X-Axis
    xtick={1,2,3},%
    xticklabels={$\vect{a}_{0}$,$\vect{a}_{1}$,$\vect{a}_{2}$},%
    %
    % Y-Axis
    ymin=1*1e-3,%
    ymax=1*1e3,%
    % ylabel={Numeric value},%
    ymode=log,%
    ymajorgrids=true,%
    axis y line*=right,%
  ]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% N = 2
% A_0
\addplot+[%
    boxplot,%
    boxplot/draw position=1-1.20*\myboxplotboxextend,%
    area legend,%
  ] table[%
      y=CD0,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n2_trials_en.dat};
% A_1
% \addplot+[%
%     boxplot,%
%     boxplot/draw position=2-1.20*\myboxplotboxextend,%
%     area legend,%
%     forget plot,%
%   ] table[%
%       y=CD1,%
%       col sep=comma,%
%     ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n2_trials_en.dat};


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% N = 3
% A_0
\addplot+[%
    boxplot,%
    boxplot/draw position=1,%
    forget plot,%
  ] table[%
      y=CD0,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n3_trials_en.dat};
% A_1
\addplot+[%
    boxplot,%
    boxplot/draw position=2,%
    area legend,%
  ] table[%
      y=CD1,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n3_trials_en.dat};
% % A_2
% \addplot+[%
%     boxplot,%
%     boxplot/draw position=3,%
%     area legend,%
%     forget plot,%
%   ] table[%
%       y=CD2,%
%       col sep=comma,%
%     ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n3_trials_en.dat};


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% N = 4
% A_0
\addplot+[%
    boxplot,%
    boxplot/draw position=1+1.20*\myboxplotboxextend,%
    forget plot,%
  ] table[%
      y=CD0,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n4_trials_en.dat};
% A_1
\addplot+[%
    boxplot,%
    boxplot/draw position=2+1.20*\myboxplotboxextend,%
    forget plot,%
  ] table[%
      y=CD1,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n4_trials_en.dat};
% A_2
\addplot+[%
    boxplot,%
    boxplot/draw position=3+1.20*\myboxplotboxextend,%
    area legend,%
  ] table[%
      y=CD2,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n4_trials_en.dat};
% % A_3
% \addplot+[%
%     boxplot,%
%     boxplot/draw position=4+1.20*\myboxplotboxextend,%
%     area legend,%
%     forget plot,%
%   ] table[%
%       y=CD3,%
%       col sep=comma,%
%     ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n4_trials_en.dat};


\end{groupplot}

\node[%
    yshift=-\subplotvsep,%
  ]%
  at ($(stress strain estimation num den trials c1r1.south)!0.5!(stress strain estimation num den trials c2r1.south)$)%
  {%
    \ref{stress strain estimation num den trials grouplegend}%
  };

\end{tikzpicture}
