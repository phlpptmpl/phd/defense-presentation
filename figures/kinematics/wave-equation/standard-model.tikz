%!TEX root=../../../thesis.tex
%!TEX file=figures/kinematics/wave-equation/standard-model.tikz

\begin{tikzpicture}[%
  ]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% coordinates
\coordinate (xi-start) at (0.00,0.00);
\coordinate (xi-end) at (10.00cm,0.00);

\coordinate (cable-start) at (0.00,0.00);
\coordinate (cable-end) at (10.00cm,0.00);
%% END coordinates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% axis
% Axis of reference
\draw[%
    Bracket-Bracket,%
  ]%
  (xi-start)%
  -- (xi-end)%
    node[%
        pos=0.00,%
        anchor=north,%
      ]%
      {$\pathcoord = 0$}%
    node[%
        pos=1.00,%
        anchor=south,%
      ]%
      {$\pathcoord = 1$};%
% Extension of axis
\draw[%
    dashed,%
  ]%
  (xi-start)%
  -- ++(-1.00cm,0.00);
\draw[%
    dashed,%
  ]%
  (xi-end)%
  -- ++(1.00cm,0.00);
%% END axis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% cable

\draw[%
    /cdpr/cable,%
    name path=cable,%
  ]%
  (cable-start)%
  .. controls (3.33cm,2.50cm) and (6.66cm,-1.50)%
  .. (cable-end);%

% A random point on the X-axis
\coordinate (xi star) at ($(xi-start)!0.28!(xi-end)$);
\draw[%
  ]%
  (xi star)%
    node[%
        anchor=north,%
      ]%
      {$\pathcoord^{\ast}$};

% Line that goes vertically up from (xi star)
\path[%
    name path=xi star up,%
  ]%
  (xi star)%
  -- ++(0.00, 2.00cm);%

% Find intersection of xi star and the cable
\path[%
    name intersections={%
      of=cable and xi star up,%
      by=pertubation xi star world,%
    },%
  ];
% Label the world deformation
\draw[%
    ->,%
  ]%
  (xi star)%
  -- (pertubation xi star world)
    node[%
        pos=1.00,%
        anchor=south,%
        /phlpptmpl/tight padding,%
        /phlpptmpl/no margin,%
      ]%
      {$\wavepertubation \of{\pathcoord^{\ast}, t}$};

%% END cable
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{tikzpicture}
