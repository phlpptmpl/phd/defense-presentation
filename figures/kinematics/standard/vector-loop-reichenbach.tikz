%!TEX root=../../../thesis.tex
%!TEX file=figures/kinematics/standard/vector-loop-reichenbach.tikz

\begin{tikzpicture}[%
    % font=\relsize{-1},%
    tdplot_main_coords,%
    line cap=round,%
    line join=miter,%
    invisible edge/.style={%
      dashed,%
      draw=PrimaryColor,%
      line cap=round,%
    },%
    visible edge/.style={%
      solid,%
    },%
    /cdpr/cable/.append style={%
      /phlpptmpl/thick,%
    },%
    /cdpr/rigid body/.append style={%
      fill opacity=0.33,%
      blend mode=normal,%
    },%
  ]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% placing coordinates

% world base
\coordinate (zero) at (0.00,0.00,-1.00);

% Frame anchor
\coordinate (A1) at (-1.00,  1.00, -1.00);
\coordinate (A2) at ( 1.00,  1.00, -1.00);
\coordinate (A3) at ( 1.00, -1.00, -1.00);
\coordinate (A4) at (-1.00, -1.00, -1.00);
\coordinate (A5) at (-1.00,  1.00,  1.00);
\coordinate (A6) at ( 1.00,  1.00,  1.00);
\coordinate (A7) at ( 1.00, -1.00,  1.00);
\coordinate (A8) at (-1.00, -1.00,  1.00);
\coordinate (ai) at (A7);

% Platform position
\coordinate (r) at (-0.50, -0.30, 0.00);
% Platform orientation
\pgfmathsetmacro{\platalpha}{-15}
\pgfmathsetmacro{\platbeta}{25}
\pgfmathsetmacro{\platgamma}{15}








%% END placing coordinates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% world coordinate system

% \node[%
%     anchor=north west,%
%     /phlpptmpl/tight padding,%
%     /phlpptmpl/no margin,%
%   ]%
%   at (zero)%
%   {$\coordsys{\symworld}$};

\draw[%
    /phlpptmpl/coordinate axis x,%
  ]%
  (zero)%
  -- ++(0.33,0.00,0.00);%
%     node[%
%         /phlpptmpl/coordinate axis label,%
%         anchor=east,%
%       ]%
%       {$\evecx[\symworld]$};

\draw[%
    /phlpptmpl/coordinate axis y,%
  ]%
  (zero)%
  -- ++(0.00,0.33,0.00);%
%     node[%
%         /phlpptmpl/coordinate axis label,%
%         anchor=west,%
%       ]%
%       {$\evecy[\symworld]$};

\draw[%
    /phlpptmpl/coordinate axis z,%
  ]%
  (zero)%
  -- ++(0.00,0.00,0.33);%
%     node[%
%         /phlpptmpl/coordinate axis label,%
%         anchor=north west,%
%       ]%
%       {$\evecz[\symworld]$};

%% END world coordinate system
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% frame

%%% Draw vertices in the background
\draw[%
    invisible edge,%
  ]%
  (A3)%
  -- (A4);
\draw[%
    invisible edge,%
  ]%
  (A4)%
  -- (A1);
\draw[%
    invisible edge,%
  ]%
  (A4)%
  -- (A8);

%%% Draw vertices in the foreground
\draw[%
    visible edge,%
  ]%
  (A1)%
  -- (A2);
\draw[%
    visible edge,%
  ]%
  (A2)%
  -- (A3);
\draw[%
    visible edge,%
  ]%
  (A1)%
  -- (A5);
\draw[%
    visible edge,%
  ]%
  (A2)%
  -- (A6);
\draw[%
    visible edge,%
  ]%
  (A3)%
  -- (A7);
\draw[%
    visible edge,%
  ]%
  (A7)%
  -- (A8);
\draw[%
    visible edge,%
  ]%
  (A8)%
  -- (A5);
\draw[%
    visible edge,%
  ]%
  (A5)%
  -- (A6);
\draw[%
    visible edge,%
  ]%
  (A6)%
  -- (A7);

%% END frame
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% platform

\tdplotsetrotatedcoords{\platalpha}{\platbeta}{\platgamma}
\tdplotsetrotatedcoordsorigin{(r)}

% Root to platform center
\draw[%
    ->,%
  ]%
  (zero)%
  -- (r);%
    % node[%
    %     pos=0.50,%
    %     anchor=north,%
    %     /phlpptmpl/tight padding,%
    %     /phlpptmpl/no margin,%
    %     sloped,%
    %     rotate=180,%
    %   ]%
    %   {$\platpos, \matr{R}$};

\begin{scope}[
    tdplot_rotated_coords,%
  ]

%%% coordinate system

\coordinate (platform-center) at (0.00,0.00,0.00);

% Place all bi
\coordinate (B1) at (-0.25,  0.30, -0.10);
\coordinate (B2) at ( 0.25,  0.30, -0.10);
\coordinate (B3) at ( 0.25, -0.30, -0.10);
\coordinate (B4) at (-0.25, -0.30, -0.10);
\coordinate (B5) at (-0.25,  0.30,  0.30);
\coordinate (B6) at ( 0.25,  0.30,  0.30);
\coordinate (B7) at ( 0.25, -0.30,  0.30);
\coordinate (B8) at (-0.25, -0.30,  0.30);
\coordinate (bi) at (B7);

% \begin{scope}[on background layer]
% %%% Draw vertices of the platform

% %%% Draw vertices in the background
% \draw[%
%     invisible edge,%
%   ]%
%   (B3)%
%   -- (B4);
% \draw[%
%     invisible edge,%
%   ]%
%   (B4)%
%   -- (B1);
% \draw[%
%     invisible edge,%
%   ]%
%   (B4)%
%   -- (B8);
  
% \end{scope}

% %%% Draw vertices in the foreground
% \draw[%
%     visible edge,%
%   ]%
%   (B1)%
%   -- (B2);
% \draw[%
%     visible edge,%
%   ]%
%   (B2)%
%   -- (B3);
% \draw[%
%     visible edge,%
%   ]%
%   (B1)%
%   -- (B5);
% \draw[%
%     visible edge,%
%   ]%
%   (B2)%
%   -- (B6);
% \draw[%
%     visible edge,%
%   ]%
%   (B3)%
%   -- (B7);
% \draw[%
%     visible edge,%
%   ]%
%   (B7)%
%   -- (B8);
% \draw[%
%     visible edge,%
%   ]%
%   (B8)%
%   -- (B5);

% Bottom face
\draw[%
    /cdpr/rigid body,%
  ]%
  (B1)%
  -- (B2)%
  -- (B3)%
  -- (B4)%
  -- cycle;%

% Left face
\draw[%
    /cdpr/rigid body,%
  ]%
  (B3)%
  -- (B4)%
  -- (B8)%
  -- (B7)%
  -- cycle;%

% Rear face
\draw[%
    /cdpr/rigid body,%
  ]%
  (B4)%
  -- (B1)%
  -- (B5)%
  -- (B8)%
  -- cycle;%

% %%% Platform coordinate system
% \node[%
%     anchor=north west,%
%     /phlpptmpl/tight padding,%
%     /phlpptmpl/no margin,%
%   ]%
%   at (platform-center)%
%   {$\mathcal{K}_{ j }$};

% % Platform x-axis
\draw[%
    /phlpptmpl/coordinate axis x,%
  ]%
  (platform-center)%
  -- ++(0.25,0.00,0.00);%
%     node[%
%         % /phlpptmpl/coordinate axis label,%
%         pos=0.75,%
%         /phlpptmpl/no padding,%
%         /phlpptmpl/no margin,%
%         anchor=north east,%
%       ]%
%       {$\evecx[\symplatform]$};

% Platform y-axis
\draw[%
    /phlpptmpl/coordinate axis y,%
  ]%
  (platform-center)%
  -- ++(0.00,0.25,0.00);%
%     node[%
%         /phlpptmpl/coordinate axis label,%
%         anchor=south west,%
%       ]%
%       {$\evecy[\symplatform]$};

% Platform z-axis
\draw[%
    /phlpptmpl/coordinate axis z,%
  ]%
  (platform-center)%
  -- ++(0.00,0.00,0.25);%
%     node[%
%         /phlpptmpl/coordinate axis label,%
%         anchor=south west,%
%       ]%
%       {$\evecz[\symplatform]$};

% Platform center to anchor
\draw[%
    ->,%
  ]%
  (r)%
  -- (bi);%
    % node[%
    %     pos=0.50,%
    %     anchor=north,%
    %     /phlpptmpl/tight padding,%
    %     /phlpptmpl/no margin,%
    %     sloped,%
    %   ]%
    %   {$\platanchor_{\cabindex}$};%


% Right face
\draw[%
    /cdpr/rigid body,%
  ]%
  (B1)%
  -- (B2)%
  -- (B6)%
  -- (B5)%
  -- cycle;%

% Top face
\draw[%
    /cdpr/rigid body,%
  ]%
  (B5)%
  -- (B6)%
  -- (B7)%
  -- (B8)%
  -- cycle;%

% Front face
\draw[%
    /cdpr/rigid body,%
  ]%
  (B2)%
  -- (B3)%
  -- (B7)%
  -- (B6)%
  -- cycle;%

% \draw[%
%     visible edge,%
%   ]%
%   (B5)%
%   -- (B6);
% \draw[%
%     visible edge,%
%   ]%
%   (B6)%
%   -- (B7);

\end{scope}

%% END platform
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% winch

% Define transformation into ai
\tdplotsetrotatedcoordsorigin{(ai)}
% Winch is rotated only a bit
\tdplotsetrotatedcoords{0}{0}{0}

% Coordinate system in Ai
\begin{scope}[
    tdplot_rotated_coords,%
  ]
  
\coordinate (ai local) at (0.00,0.00,0.00);

% \draw[%
%     /phlpptmpl/coordinate axis x,%
%   ]%
%   (ai local)%
%   -- ++(0.50,0.00,0.00);%
%     % node[%
%     %     /phlpptmpl/coordinate axis label,%
%     %     anchor=north,%
%     %   ]%
%     %   {$\evecx[\symwinch]$};%
% \draw[%
%     /phlpptmpl/coordinate axis y,%
%   ]%
%   (ai local)%
%   -- ++(0.00,0.25,0.00);%
%     % node[%
%     %     /phlpptmpl/coordinate axis label,%
%     %     anchor=north,%
%     %   ]%
%     %   {$\evecy[\symwinch]$};%
% \draw[%
%     /phlpptmpl/coordinate axis z,%
%   ]%
%   (ai local)%
%   -- ++(0.0,0.00,0.25);%
%     % node[%
%     %     /phlpptmpl/coordinate axis label,%
%     %     anchor=east,%
%     %   ]%
%     %   {$\evecz[\symwinch]$};%

\end{scope}

%% END winch
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% connections and labels

% root to Ai
\draw[%
    ->,%
  ]%
  (zero)%
  -- (ai);%
    % node[%
    %     pos=0.50,%
    %     anchor=north,%
    %     /phlpptmpl/tight padding,%
    %     /phlpptmpl/no margin,%
    %     sloped,%
    %   ]%
    %   {$\frameanchor_{\cabindex}$};
% % Label Ai
% \node[%
%     anchor=south east,%
%     /phlpptmpl/tight padding,%
%     /phlpptmpl/no margin,%
%   ]%
%   at (ai)%
%   {$\Ai$};

% % Label Bi
% \node[%
%     anchor=north east,%
%     /phlpptmpl/tight padding,%
%     /phlpptmpl/no margin,%
%   ]%
%   at (bi)%
%   {$\Bi$};

% Cable
\draw[%
    ->,
    /cdpr/cable,%
  ]%
  (bi)%
  -- (ai);%
    % node[%
    %     pos=0.50,%
    %     anchor=south,%
    %     /phlpptmpl/tight padding,%
    %     /phlpptmpl/no margin,%
    %     sloped,%
    %   ]%
    %   {$\cabdirn_{\cabindex}$};
% Cable unit vector
\draw[%
    ->,%
    semithick,%
  ]%
  (bi)%
  -- ($(bi)!0.25!(ai)$);%
    % node[%
    %     pos=0.50,%
    %     anchor=south east,%
    %     /phlpptmpl/tight padding,%
    %     /phlpptmpl/no margin,%
    %     sloped,%
    %   ]%
    %   {$\cabudirn_{\cabindex}$};

%% END connections and labels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
\end{tikzpicture}
