%!TEX root=../../../../thesis.tex
%!TEX file=figures/cosserat-rod/bsplines/catenary-comparison/radius-1d00.tikz

%\setlength{\subplothsep}{1.00\baselineskip}
%\setlength{\subplotvsep}{2.50\baselineskip}
\setlength{\subplotwidth}{1.00\linewidth/3 - 2\subplothsep}
\setlength{\subplotheight}{0.90\subplotwidth}

\begin{tikzpicture}[
  ]

\begin{groupplot}[%
    group style={%
      % General stuff
      group name={catenary comparison residual integral radius 1d00},%
      % Layout
      columns=3,%
      rows=1,%
      horizontal sep=\subplothsep,%
      vertical sep=\subplotvsep,%
      % X-Axes
      xlabels at=edge bottom,%
      xticklabels at=edge bottom,%
      % Y-Axes
      ylabels at=edge left,%
      yticklabels at=edge left,%
    },%
    %
    % General adjustments
    catenary error residual area norm,%
    width=\subplotwidth,%
    height=\subplotheight,%
    %
    % X-Axis
    %
    % Y-Axis
    ymax=0.060,%
  ]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 0 degree angle
\nextgroupplot[%
    title={$\beta_{\funcindex_{2}} = \SI{0}{\degree}$},%
    % Legend style
    legend to name={catenary comparison residual integral radius 1d00 grouplegend},%
    legend style={%
      legend columns=-1,%
      /tikz/every even column/.append style={%
        column sep=1.00ex,%
      },%
    },%
  ]

\foreach \ord in {2,3,4,5,6}{%
  \addplot+[%
    ]table[%
        col sep=comma,%
        x=ns,%
        y=d\ord,%
      ]{data/cosserat-rod/bsplines/catenary-comparison/angle-0deg/radius-1.00.dat};
  \addlegendentryexpanded{$\polydeg = \ord$};
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 144 degree angle
\nextgroupplot[%
    title={$\beta_{\funcindex_{2}} = \SI{144}{\degree}$},%
  ]

\foreach \ord in {2,3,4,5,6}{%
  \addplot+[%
    ]table[%
        col sep=comma,%
        x=ns,%
        y=d\ord,%
      ]{data/cosserat-rod/bsplines/catenary-comparison/angle-144deg/radius-1.00.dat};
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 288 degree angle
\nextgroupplot[%
    title={$\beta_{\funcindex_{2}} = \SI{288}{\degree}$},%
  ]

\foreach \ord in {2,3,4,5,6}{%
  \addplot+[%
    ]table[%
        col sep=comma,%
        x=ns,%
        y=d\ord,%
      ]{data/cosserat-rod/bsplines/catenary-comparison/angle-288deg/radius-1.00.dat};
}

\end{groupplot}

\node[%
    yshift=-1.25\subplotvsep,%
  ]%
  at ($(catenary comparison residual integral radius 1d00 c1r1.south)!0.5!(catenary comparison residual integral radius 1d00 c3r1.south)$)%
  {%
    \ref{catenary comparison residual integral radius 1d00 grouplegend}%
  };

\end{tikzpicture}
