%!TEX root=../source.tex
%!TEX file=figures/mechatronic-system-model-simple.tikz

\begin{tikzpicture}[%
    node distance=4.00em and 12.00em,%
    on grid,%
    /phlpptmpl/mindmap node/.append style={%
      text width=5.00em,%
      minimum height=3.25em,%
      transform shape,%
    },%
    /phlpptmpl/mindmap line/.append style={
      ->,%
    },%
    /phlpptmpl/mindmap line annotation/.append style={%
    },%
  ]



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% nodes

\node[%
    /phlpptmpl/mindmap node,%
  ]%
  (numerical control)%
  {\trans{Numerical Control}{Numerische Steuerung}};

\node[%
    /phlpptmpl/mindmap node,%
    right=of numerical control,%
    rectangle split,%
    rectangle split horizontal,%
    rectangle split parts=2,%
  ]%
  (drive train)%
  {\trans{Servo-motor}{Servomotor}\nodepart{two}\trans{Winch}{Winde}};

\node[%
    /phlpptmpl/mindmap node,%
    right=of drive train,%
  ]%
  (cables)%
  {\trans{Cables}{Seile}};

\node[%
    /phlpptmpl/mindmap node,%
    coordinate,%
    below=of cables,%
  ]%
  (below cables)%
  {%
  };

\node[%
    right=of cables,%
    /phlpptmpl/mindmap node,%
  ]%
  (platform)%
  {\trans{Platform}{Plattform}};

\node[%
    below=of platform,%
    /phlpptmpl/mindmap node,%
    coordinate,%
    draw=none,%
    fill=none,%
  ]%
  (below platform)%
  {};

\node[%
    right=of platform,%
    /phlpptmpl/mindmap node,%
  ]%
  (environment)%
  {\trans{Environment}{Umgebung}};

\node[%
    below=of environment,%
    /phlpptmpl/mindmap node,%
    coordinate,%
    draw=none,%
    fill=none,%
  ]%
  (below environment)%
  {};

%% END nodes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% connections

\draw[%
    /phlpptmpl/mindmap vector,%
  ]%
  (numerical control)%
  -- (drive train)%
  node[%
      /phlpptmpl/mindmap line annotation,%
      pos=0.33,%%
      anchor=south,%
    ]%
    {$\actuang_{\ms{\trans{cmd}{strg}}}$};

\draw[
    /phlpptmpl/mindmap vector,%
    arrows={%
      -Stealth[harpoon]%
    },%
    transform canvas={%
      shift={( 0.00, 0.50em)},%
    },%
  ]%
  (drive train)%
  -- (cables)%
    node[%
        /phlpptmpl/mindmap line annotation,%
        pos=0.50,%
        anchor=south,%
      ]%
      {$\actuang_{\subwinch}$};

\draw[
    /phlpptmpl/mindmap vector,%
    arrows={%
      -Stealth[harpoon]%
    },%
    transform canvas={%
      shift={( 0.00,-0.50em)},%
    },%
  ]%
  (cables)
  -- (drive train)%
    node[%
        /phlpptmpl/mindmap line annotation,%
        pos=0.50,%
        anchor=north,%
      ]%
      {$\cabforces$};

\draw[%
    /phlpptmpl/mindmap vector,%
  ]%
  (cables)%
  -- (platform)%
    node[
        /phlpptmpl/mindmap line annotation,%
        pos=0.50,%%
        anchor=south,%
      ]%
      {$\cabforces$};

\draw[%
    /phlpptmpl/mindmap vector,%
  ]%
  (environment)%
  -- (platform)%
    node[%
        /phlpptmpl/mindmap line annotation,%
        pos=0.33,%%
        anchor=south,%
      ]%
      {$\wrench_{\symgravity}, \wrench_{\subprocess}$};

\draw[%
    /phlpptmpl/mindmap vector,%
  ]%
  (platform)%
  -- (below platform)%
  -- (below environment)%
    node[%
        /phlpptmpl/mindmap line annotation,%
        pos=1.00,%
        anchor=west,%
      ]%
      {$\platpose, \dotplatpose, \ddotplatpose$};%

%% END connections
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% fits

% \begin{scope}[on background layer]

% \node[%
%     /phlpptmpl/mindmap collection node,%
%     fit=(cascaded control) (below cascaded control) (electrodynamics) (ammeter),%
%     inner ysep=1.00em,%
%     inner xsep=0.50em,%
%   ]%
%   (electrical subsystem)%
%   {};
% \node[%
%     annotation,%
%     anchor=north west,%
%   ]%
%   at (electrical subsystem.south west)%
%   {\trans{Electrical Subsystem}{Elektrisches Teilsystem}};%

% \node[%
%     /phlpptmpl/mindmap collection node,%
%     fit=(winch mechanics) (position encoder) (cables) (platform) (inverse kinematics),%
%     inner ysep=1.00em,%
%     inner xsep=0.50em,%
%   ]%
%   (mechanical subsystem)%
%   {};
% \node[%
%     annotation,%
%     anchor=north west,%
%   ]%
%   at (mechanical subsystem.south west)%
%   {\trans{Mechanical Subsystem}{Mechanisches Teilsystem}};%

% \end{scope}

%% END fits
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{tikzpicture}
