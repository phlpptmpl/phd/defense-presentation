%!TEX root=../../thesis.tex
%!TEX file=figures/material/generalized-maxwell.tikz

\providelength{\gmwgroundwidth}
\providelength{\gmwgroundheight}
\providelength{\gmwelementlength}
\providelength{\tikztikzmechsgrounddiagonal}
\providelength{\tikztikzmechsgroundspacing}

\setlength{\gmwgroundwidth}{4.00cm}
\setlength{\gmwgroundheight}{1.00cm}
\setlength{\gmwelementlength}{2.00cm}

\begin{tikzpicture}[
    % /ustutt/semithick,%
  ]


\begin{scope}[%
    % rotate=90,%
  ]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% coordinates

\coordinate%
  (element start)
  at (0.00,0.00);
\coordinate%
  (gmw element start)%
  at ($(element start) + (0.50\gmwelementlength,0.00)$);
\coordinate%
  (gmw element end)%
  at ($(gmw element start) + (2.00\gmwelementlength, 0.00)$);
\coordinate%
  (element end)%
  at ($(gmw element end) + (0.50\gmwelementlength,0.00)$);

%% END coordinates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ground

% Ground rectangle
\draw[%
  ]%
  (element start)%
  ++ (0.00cm,-0.50\gmwgroundwidth)%
  -- ++(0.00cm,1.00\gmwgroundwidth);

% Clipping for the ground hatching
\begin{scope}
  \clip%
    (element start)%
    ++ (-\gmwgroundheight,-0.50\gmwgroundwidth)%
    rectangle ++(\gmwgroundheight, 1.00\gmwgroundwidth);
  
  % Calculate end of the hatching loop
  \pgfmathtruncatemacro\tikztikzmechsgroundloopend{10}%
  % Distance between hatching lines
  \setlength\tikztikzmechsgroundspacing{0.10\gmwgroundwidth}%
  % Calculate the length of the diagonal of hatching
  \pgfmathsetlengthmacro\pgftikzmechsgrounddiagonal{sqrt( ( \gmwelementlength )^2 + ( \gmwelementlength )^2 )}%
  \setlength\tikztikzmechsgrounddiagonal{\pgftikzmechsgrounddiagonal}%
  
  \foreach \nline in {0,...,\tikztikzmechsgroundloopend} {%
    \draw[%
        draw,%
      ]%
      (-1.00\gmwgroundheight, -0.50\gmwgroundwidth)%
        ++(45:{(\nline+0.5)*\tikztikzmechsgroundspacing})%
        ++({45-90}:\tikztikzmechsgrounddiagonal)%
      -- ++({45+90}:2\tikztikzmechsgrounddiagonal);%
  }%
  
\end{scope}

%% END ground
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% elemt surrounding

% Start bar top and bottom
\coordinate%
  (start bar start)%
  at ($(gmw element start) + (0.00,-0.50\gmwgroundwidth)$);
\coordinate%
  (start bar end)%
  at ($(gmw element start) + (0.00,0.50\gmwgroundwidth)$);

% End bar top and bottom
\coordinate%
  (end bar start)%
  at ($(gmw element end) + (0.00,-0.50\gmwgroundwidth)$);
\coordinate%
  (end bar end)%
  at ($(gmw element end) + (0.00,0.50\gmwgroundwidth)$);

% Connection to material bar
\draw[%
    -,%
  ]%
  (element start)%
  -- (gmw element start);

% Start bar of material
\draw[%
    -,%
    /ustutt/thick,%
    shorten >=-0.02\gmwelementlength,%
    shorten <=-0.02\gmwelementlength,%
  ]%
  (start bar start)%
  -- (start bar end);

% End bar of material
\draw[%
    -,%
    /ustutt/thick,%
    shorten >=-0.02\gmwelementlength,%
    shorten <=-0.02\gmwelementlength,%
  ]%
  (end bar start)%
  -- (end bar end);

% Connection bar to force
\draw[%
    -,%
  ]%
  (gmw element end)%
  -- (element end);

%% END element surrounding
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% maxwell material components' coordinates

% Pure spring
\coordinate%
  (spring start)%
  at ($(start bar start)!0.01!(start bar end)$);
\coordinate%
  (spring end)%
  at ($(end bar start)!0.01!(end bar end)$);

% First MW element
\coordinate%
  (mw one start)%
  at ($(start bar start)!0.27!(start bar end)$);
\coordinate%
  (mw one end)%
  at ($(end bar start)!0.27!(end bar end)$);
\coordinate%
  (mw one spring start)%
  at ($(mw one start) + (0.00,0.00)$);
\coordinate%
  (mw one spring end)%
  at ($(mw one spring start) + (1.00\gmwelementlength,0.00)$);
\coordinate%
  (mw one damper start)%
  at ($(mw one spring end) + (0.00,0.00)$);
\coordinate%
  (mw one damper end)%
  at ($(mw one damper start) + (1.00\gmwelementlength,0.00)$);

% Second MW element
\coordinate%
  (mw two start)%
  at ($(start bar start)!0.54!(start bar end)$);
\coordinate%
  (mw two end)%
  at ($(end bar start)!0.54!(end bar end)$);
\coordinate%
  (mw two spring start)%
  at ($(mw two start) + (0.00,0.00)$);
\coordinate%
  (mw two spring end)%
  at ($(mw two spring start) + (1.00\gmwelementlength,0.00)$);
\coordinate%
  (mw two damper start)%
  at ($(mw two spring end) + (0.00,0.00)$);
\coordinate%
  (mw two damper end)%
  at ($(mw two damper start) + (1.00\gmwelementlength,0.00)$);

% Third MW element
\coordinate%
  (mw three start)%
  at ($(start bar start)!0.80!(start bar end)$);
\coordinate%
  (mw three end)%
  at ($(end bar start)!0.80!(end bar end)$);
\coordinate%
  (mw three spring start)%
  at ($(mw three start) + (0.00,0.00)$);
\coordinate%
  (mw three spring end)%
  at ($(mw three spring start) + (1.00\gmwelementlength,0.00)$);
\coordinate%
  (mw three damper start)%
  at ($(mw three spring end) + (0.00,0.00)$);
\coordinate%
  (mw three damper end)%
  at ($(mw three damper start) + (1.00\gmwelementlength,0.00)$);

% Fourth/Last MW element
\coordinate%
  (mw last start)%
  at ($(start bar start)!0.99!(start bar end)$);
\coordinate%
  (mw last end)%
  at ($(end bar start)!0.99!(end bar end)$);
\coordinate%
  (mw last spring start)%
  at ($(mw last start) + (0.00,0.00)$);
\coordinate%
  (mw last spring end)%
  at ($(mw last spring start) + (1.00\gmwelementlength,0.00)$);
\coordinate%
  (mw last damper start)%
  at ($(mw last spring end) + (0.00,0.00)$);
\coordinate%
  (mw last damper end)%
  at ($(mw last damper start) + (1.00\gmwelementlength,0.00)$);

%% END maxwell material components' coordinates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% maxwell material components'

\spring[%
    length=0.50\gmwelementlength,%
    amplitude=0.08\gmwelementlength,%
    periods=4,%
    name=spring pure,%
  ]%
  {(spring start)}%
  {(spring end)};

\spring[%
    length=0.50\gmwelementlength,%
    amplitude=0.08\gmwelementlength,%
    periods=4,%
    name=spring one,%
  ]%
  {(mw one spring start)}%
  {(mw one spring end)};

\spring[%
    length=0.50\gmwelementlength,%
    amplitude=0.08\gmwelementlength,%
    periods=4,%
    name=spring two,%
  ]%
  {(mw two spring start)}%
  {(mw two spring end)};

\spring[%
    length=0.50\gmwelementlength,%
    amplitude=0.08\gmwelementlength,%
    periods=4,%
    name=spring last,%
  ]%
  {(mw last spring start)}%
  {(mw last spring end)};

\damper[%
    length=0.10\gmwelementlength,%
    width=0.08\gmwelementlength,%
    dashpot width=0.06\gmwelementlength,%
    name=damper one,%
  ]%
  {(mw one damper start)}%
  {(mw one damper end)};

\damper[%
    length=0.10\gmwelementlength,%
    width=0.08\gmwelementlength,%
    dashpot width=0.06\gmwelementlength,%
    name=damper two,%
  ]%
  {(mw two damper start)}%
  {(mw two damper end)};

\damper[%
    length=0.10\gmwelementlength,%
    width=0.08\gmwelementlength,%
    dashpot width=0.06\gmwelementlength,%
    name=damper last,%
  ]%
  {(mw last damper start)}%
  {(mw last damper end)};

\draw[%
    dotted,%
  ]%
  (mw three damper start)%
  ++ (0.00cm,-0.20\gmwelementlength)%
  -- ++(0.00cm,0.40\gmwelementlength);

%% END maxwell material components'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% annotations

% "break" in the left-hand and right-hand bar
\draw[
    dashed,%
    draw=White,%
    /ustutt/very thick,%
  ]%
  ($(mw three start)+(0.00,-0.20\gmwelementlength)$)%
  -- ++(0.00,0.40\gmwelementlength);
\draw[
    dashed,%
    draw=White,%
    /ustutt/very thick,%
  ]%
  ($(mw three end)+(0.00,-0.20\gmwelementlength)$)%
  -- ++(0.00,0.40\gmwelementlength);

% Force input
\draw[%
    ->,%
    shorten <=0.02\gmwelementlength,%
  ]%
  (element end)%
  -- ++(0.50\gmwelementlength, 0.00cm)%
    node[%
        pos=1.00,%
        anchor=west,%
        /ustutt/normal padding,%
        /ustutt/no margin,%
      ]%
      {$\stress*$};%
% Elongation measurement
\draw[%
    ->,%
    shorten <=-0.50mm,%
  ]%
  (element end)%
  -- ++(0.00cm, -1.50\baselineskip)% A little down shift
  -- ++(0.50\gmwelementlength, 0.00cm)%
    node[%
        pos=1.00,%
        anchor=west,%
        /ustutt/normal padding,%
        /ustutt/no margin,%
      ]%
      {$\strain*$};%

% Youngs Moduli
\draw[%
  ]%
  (spring pure.north)%
    node[%
        /ustutt/tight padding,%
        /ustutt/no margin,%
        anchor=south,%
      ]%
      {$\youngsmodulus_{1}$};
\draw[%
  ]%
  (spring one.north)%
    node[%
        /ustutt/tight padding,%
        /ustutt/no margin,%
        anchor=south,%
      ]%
      {$\youngsmodulus_{2}$};
\draw[%
  ]%
  (spring two.north)%
    node[%
        /ustutt/tight padding,%
        /ustutt/no margin,%
        anchor=south,%
      ]%
      {$\youngsmodulus_{3}$};
\draw[%
  ]%
  (spring last.north)%
    node[%
        /ustutt/tight padding,%
        /ustutt/no margin,%
        anchor=south,%
      ]%
      {$\youngsmodulus_{n_{\msmaterial}}$};

% Kelvin-Voigt viscous moduli
\draw[%
  ]%
  (damper one.north)%
    node[%
        /ustutt/tight padding,%
        /ustutt/no margin,%
        anchor=south,%
      ]%
      {$\viscousmodulus_{2}$};
\draw[%
  ]%
  (damper two.north)%
    node[%
        /ustutt/tight padding,%
        /ustutt/no margin,%
        anchor=south,%
      ]%
      {$\viscousmodulus_{3}$};
\draw[%
  ]%
  (damper last.north)%
    node[%
        /ustutt/tight padding,%
        /ustutt/no margin,%
        anchor=south,%
      ]%
      {$\viscousmodulus_{n_{\msmaterial}}$};

%% END annotations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{scope}

\end{tikzpicture}
