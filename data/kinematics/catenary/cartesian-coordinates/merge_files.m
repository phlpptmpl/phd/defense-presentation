function merge_files()
% MERGE_FILES Merge all DAT files into one big DAT file



%% File information
% Author: Philipp Tempel <philipp.tempel@isw.uni-stuttgart.de>
% Date: 2019-07-14
% Changelog:
%   2019-07-14
%       * Initial release



%% Do your code magic here

% Get all files
stFiles = allfiles(fileparts(mfilename('fullpath')), '*.dat');

% Number of files
nFiles = numel(stFiles);

% Store loaded data
ceData = cell(1, nFiles);

% Load all data
for iFile = 1:nFiles
  ceData{iFile} = readtable(fullfile(stFiles(iFile).folder, stFiles(iFile).name));
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header Your contribution towards improving this function
% will be acknowledged in the "Changes" section of the header
